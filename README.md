<!-- @format -->

# Progetto per il corso di sistemi operativi

## Arduino con web server

### PC_Server

Avrà un interfaccia web e un interfaccia terminale, i led/adv saranno addressabili da terminale, mentre l'interfaccia web avrà solo la possibilità di accedere/spegnere/dimmerare i led e mostrare i valori che hanno gli adv

### Arduino

La comunicazione sarà tramite uart con interrupt, potenziometri e led dimmerabili, ci saranno anche tre pin pwm uniti per formare un led rgb, non addressabili che riveleranno alcune statistiche sul pc ( cpu load , pc status ,...)

## ToDo

-   [x] Arduino
    -   [x] receive and check commands
        -   [x] bit + checksum
            -   [x] send to pins
                -   [x] init pins
-   [x] PC
    -   [x] comunicazione con arduino
        -   [x] uart communication
            -   [x] bitwise + checksum
    -   [x] rgb
        -   [x] multiple mode
            -   [x] police
            -   [x] static led
            -   [x] informazioni sul cpu
                -   [x] interpreta /proc/stat
    -   [x] libwebsocket
        -   [x] understand libwebsocket
        -   [x] Browser
            -   [x] interfaccia per led
            -   [x] accendi, spegni dimmera
            -   [x] check analog status (live-almost)
            -   [x] check digital in status
            -   [x] index.html + js

## How to use

### Req

This project needs to have libwebsocket (3.2) installed and avr-gcc,avrdude

### Start Using It


Download this git folder, then run "make" command inside the proj folder to build the program for pc and to flash the arduino.
Or download last release if u can not use libwebsocket and follow the instructions on the release page 

```
$ git clone $(this)
$ cd $(git folder)/proj
$ make
$ cd pc
$ ./smarthouse -h
```

Then in the pc folder you will have to start "smarthouse"
raccomanded to use smarthouse -h to know something more

Is needed to run the program from his folder because the position of needed files are relative
