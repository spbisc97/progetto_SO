<!-- @format -->

# Arduino smart house

## WHAT

This is a smart house client-server application to control have a basic control on house led and sensors.

This means we have a program on the arduino and a program on pc, those two devices can communicate using serial.
The serial communication in binary, in particular in this case one or more byte is an instruction or a payload for the instruction, there is also a sanity check for the information.
At the end of the message we have a single byte checksum ,if the checksum is wrong all the message is trashed and an error is sent to the other device.

### Arduino

We had to expose 8 ports for pwm led control,and we used 8 + 3 , 8 can be individually modified and 3 are composed to create an rgb controller.
We used Digital pin 2 to 9 for individually controlled led and pins 10 to 12 for the associated 3 pins rgb led(cathode based rgb led )

We had to expose 8 ports for analog input, analog reader coulb be easily used to read light level, or humidity, or also as a simple pot reader.
We used Analog pins from 0 to 8.

We had to expose 8 ports for a digital input, for a button or a lever. So we exposed 4 and 4. Digital pins from 46 to 49 are used as levers, and Digital pins from 50 to 53 are used as buttons.

## HOW

The entire idea is based on using the arduino as an echo server(not the best idea retrospectively).
This means that to get an answer you have to send a request, so is needed to have someone(pc) continuously keep asking "how is going".
Arduino parses the commands on the buffer sent, each command is a byte,and can or not have a payload.

-   1 arduino is connected and is always waiting for info
-   2 pc program is launched, tryes to start serial communication with arduino on ttyACM0 if not --device "x" used
-   3 if connection is working launch the notification "yad" if -n (not notify) not selected
-   3 init cpu_usage mode , start handling signals and print my info
-   4 parse the config file (you can also modify config after program is launched)
-   5 let the websocket thread start and jump in the main while
-   6 check what kind of rgb feature is selected and update to Arduino if you have to
-   7 check the status for buttons and analog input (if you have to)
-   8 change leds behaviour if you need !
-   9 if stop value is selected exit from the main
-   10 repeat from 6
-   11 if you are here stop was selecting and we are going to kill the program
-   12 close the thread and clean everything on it
-   13 say to the Arduino to close everything
-   14 close the notification if you have to

## HOW TO RUN

```
    Requirements :
    1. Avr-gcc
    2. gcc
    3. Libwebsockets library (3.2) , LibEv, LibUv
    4. Yad (sudo apt install yad) for the notification
```

-   1. Copy the 'master' branch
    -   _If is not possible to install the right version of Libwebsockets use 'built' branch with prebuilt static libs binaries(any branch you will download you need to follow this instructions)_
-   2. Connect arduino through usb
-   3. Inside proj folder run 'make' command (to build the pc- elf and flash the arduino)
-   4. Go inside the pc folder and run smarthouse program ("smarthouse -h" to know more)
-   5. Access localhost:7681 and follow the instruction to config _smarthouse.conf_
