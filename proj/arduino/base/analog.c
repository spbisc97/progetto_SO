#include "analog.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

void analog_init(void) {
    //! admux will set the reference
    //! and the pin we are checking
    //! no need to set those as input in DDRF
    ADMUX |= (1 << REFS0);  //? reference voltage 5v
    PORTF = 0xFF;           //! we need all the 7 pins of port f with pullup

    ADCSRA |= _BV(ADEN);  //? enables adc
    // ADCSRA |= _BV(ADIE);  //? enables interrupt

    ADCSRA |= _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);  //! prescaler 128
}
int analog_read(uint8_t channel) {
    //! here we need to clean admux first bits to check for the new channel
    //! for a new channel volage
    ADMUX &= 0xF0;

    //! we want to use only a part of the analog pins: from 0 to 7
    ADMUX |= channel & 0x07;

    ADCSRA |= _BV(ADSC);  //! this starts the conversion (reads the channel )
    while (ADCSRA & _BV(ADSC)) {
        //! converting
        ;
    }
    return ADCW;
    //! returns the wide value (10 )
}

//? testing  main
#ifdef MAIN_ANALOG_TESTING
#include "../avr_common/uart.h"
int main(void) {
    analog_init();
    printf_init();
    // analog_read(0);
    // sei();
    while (1) {
        _delay_ms(200);
        int value = analog_read(0);
        printf(" -light %d- \n", value);
        value = analog_read(2);
        printf(" -pot %d- \n", value);
    }
}
#endif  // MAIN_ANALOG_TESTING

//! ISR(ADC_vect) {
//!    ADCSRA |= (1 << ADSC);
//!
//!    printf(" -%d- \n", ADCW);
//!    _delay_ms(500);
//!}