#include "harder_echo.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>  //for port definitions
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

#include "analog.h"
#include "buttons.h"
#include "rgb.h"

//!  _BV(n) (1 << n)

uint8_t maybe_last;  // to keep track of ending values

uint8_t sumtx = 0, sumrx = 0;

UART uart;  // defined struct to keep some uart buffers and values

uint8_t value = 200;

void filter(void) {
    UCSR0B &= ~_BV(RXCIE0);
    //! here there is a basic checksum
    uint8_t pos = uart.rxpos;
    sumrx = 0;
    while (pos < (uart.rxlen - 2)) {
        sumrx += uart.rx_buf[pos];
        pos++;
    }
    //! we sum all the bytes and we check if the rest of the division is right
    if (uart.rx_buf[pos] != (sumrx % 4)) {
        uart.tx_buf[uart.txlen++] = ERROR;
        uart.tx_buf[uart.txlen++] = ERROR;
        uart.rxpos = uart.rxlen = 0;
        _delay_ms(10);
        UCSR0B |= _BV(UDRIE0) | _BV(RXCIE0);
        return;
    }
    //! if we are here this means the checksum was right
    while (uart.rxpos < uart.rxlen) {
        if (uart.rxpos < (MAX_BUF - 3) &&
            uart.rx_buf[uart.rxpos] == (uint8_t)0xda) {  //! 0xda -> noeffect
            uart.tx_buf[uart.txlen++] = 'y';  //! 'y'->0x79 mode changed
            uart.rxpos++;
            effect_stop();  // stop effects if there was some active
            uint8_t red = uart.rx_buf[uart.rxpos++];  // just read color values
            uint8_t green = uart.rx_buf[uart.rxpos++];
            uint8_t blue = uart.rx_buf[uart.rxpos++];
            rgb_set(red, green, blue);  // set color values
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0xdb) {  //! 0xdb -> cpu_
            uart.tx_buf[uart.txlen++] = 'y';  //! 'y'->0x79 mode changed
            uart.rxpos++;
            // or compare?
            rgb.mode = cpu_usage;
            uint8_t red = uart.rx_buf[uart.rxpos++];
            uint8_t green = uart.rx_buf[uart.rxpos++];
            uint8_t blue = uart.rx_buf[uart.rxpos++];
            //! just set the colors
            rgb_set_override(red, green, blue);
            rgb.time = 0;
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0xdc) {
            //! 0xdc -> start policemode
            uart.tx_buf[uart.txlen++] = 'y';  //! 'y'->0x79 mode changed
            uart.rxpos++;
            effect_police();
            uart.rxpos++;
            rgb.time = 0;
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0xdd) {
            //! 0xdd -> analogread
            uart.tx_buf[uart.txlen++] = (uint8_t)0xdd;
            uart.rxpos++;
            uart.tx_buf[uart.txlen++] =
                (uint8_t)(analog_read(uart.rx_buf[uart.rxpos]) >> 2);
            uart.rxpos++;
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0xde &&
                   uart.rxpos < (MAX_BUF - 3)) {
            //! 0xdd -> set pwm leds
            uart.tx_buf[uart.txlen++] = (uint8_t)0xde;
            uart.rxpos++;
            uint8_t temp = uart.rx_buf[uart.rxpos++];
            uart.tx_buf[uart.txlen++] =
                (uint8_t)(set_pwm(uart.rx_buf[uart.rxpos++], temp));

            //? set_pwm(uart.rx_buf[uart.rxpos++], uart.rx_buf[uart.rxpos++]));
            //? this shorter operation could word different on different
            //? compilers
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0xdf) {
            //! 0xdf -> read buttons and levers
            //! from 50 to 53 are buttons , from 46 to 49 are levers
            uart.tx_buf[uart.txlen++] = (uint8_t)0xdf;
            check_levers();
            uart.rxpos++;
            uart.tx_buf[uart.txlen++] = buttons_status;
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0x3f) {
            //! 0x3f -> stop execution
            DDRB = 0;
            cli();
            stop = 1;
        } else {
            //! this in reality makes no sense
            uart.tx_buf[uart.txlen++] = uart.rx_buf[uart.rxpos++];
        }
    }
    //! clean everything and start as clean as new
    uart.rxpos = uart.rxlen = 0;
    _delay_ms(10);
    //// //? seems to be better to append from here the return values
    //// int position = 0;
    //// int sums = 0;
    //// while (position < uart.txlen) {
    ////     sums += uart.tx_buf[position];
    ////     uart.tx_buf[position++];
    //// }
    //// uart.tx_buf[uart.txlen++] = sumtx % 4;
    //// //?
    UCSR0B |= _BV(UDRIE0) | _BV(RXCIE0);
}

void UART_init(void) {
    uart.rxpos = 0;
    uart.txpos = 0;
    uart.txlen = 0;
    uart.rxlen = 0;
    // Set baud rate
    UBRR0H = (uint8_t)(MYUBRR >> 8);
    UBRR0L = (uint8_t)MYUBRR;

    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); /* 8-bit data */
    /* Enable     RX    and    TX and  interr-RX and interr-free  and
     * interr-tx-complete  */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0);  // | _BV(TXCIE0);
};

// rx interrupt service routine
ISR(USART0_RX_vect) {
    // read the UART register UDR0
    uint8_t c = UDR0;
    // copies values from UART register to common rx_buffer
    uart.rx_buf[uart.rxlen++] = c;
    if (maybe_last) {
        //! really stupid...
        if (0x0F == (uint8_t)c) {
            filter();
        }
        maybe_last = 0;
    }
    //! to have more chanche this is not taken
    //! i'm scared of adding too much not needed computation
    if (c < 0x04) maybe_last = 1;
}
// tx interrupt service routine
ISR(USART0_UDRE_vect) {  // copies values from common tx_buffer to UART register
                         // while there is ancora buffer da scrivere scrivi

    if (uart.txpos < uart.txlen) {
        sumtx += uart.tx_buf[uart.txpos];
        UDR0 = uart.tx_buf[uart.txpos++];
    } else {
        //! send cechsum
        UDR0 = sumtx % 4;
        sumtx = 0;
        uart.txpos = uart.txlen = 0;
        UCSR0B &= ~_BV(UDRIE0);
    }
}

// ISR(USART0_TX_vect) {
//     if (uart.txlen == 0) {
//         UCSR0B &= ~_BV(UDRIE0) | ~_BV(TXCIE0);
//     }
// }
