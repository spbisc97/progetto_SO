#include <stdint.h>

typedef struct {
    char rx_buf[256];
    char tx_buf[256];
    uint8_t rxpos;
    uint8_t rxlen;
    uint8_t txpos;
    uint8_t txlen;
} UART;
// void move_trought(void);
void UART_init(void);


#define ERROR 0x11;
#define BAUD 19200
#define MYUBRR (F_CPU / 16 / BAUD - 1)
#define MAX_BUF 256

int stop;
//! filter function

//? 0xda -> means set rgb 
//? and takes the next tree values 
//? to set the level 
//? same for 0xdb -> but this also sets cpu usage mode