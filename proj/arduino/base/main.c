#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <util/delay.h>

#include "analog.h"
#include "buttons.h"
#include "harder_echo.h"
#include "rgb.h"

int stop = 0;
int main(void) {
    cli();
    init_rgb();
    //! inizializza le porte necessarie per la striscia rgb
    init_buttons();
    //! inizializza le porte tra dalla 50 alla 53 per i bottoni
    init_levers();
    //! inizializza le porte tra la 46 alla 49 per le leve
    UART_init();
    //! inizializza la comuncazione uart
    analog_init();
    //! inizializza i pin analogici
    init_timer5();
    //! inizializza il timer5 per vari confronti es nella cpu mode
    sei();
    //! enable interrupts
    _delay_ms(10);

    for (;;) {
        if (stop) break;
    }
}