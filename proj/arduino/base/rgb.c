#include "rgb.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

#include "harder_echo.h"

//  in this function we will init some bits for pwm in
//!  oc1a oc1b oc2a -> pin 12,11,10 -> B6,7,8

//! we will use pins from 2 to 9 for single pwm control
//! list:
//!   2  -   3  -   4  -   5  -   6  -   7  -   8  -   9
//! OC3B - OC3C - OC0B - OC3A - OC4A - OC4B - OC4C - OC2B

void init_rgb(void) {
    //! this will init rgb and pwm
    // we will use timer  0
    TCCR0A |= _BV(WGM20);   // wawe generation mode
    TCCR0A |= _BV(COM0B1);  //| _BV(WGM21);  // compare output
    TCCR0B |= _BV(CS20);    // no presc //! pin 4

    // we will use timer 1
    TCCR1A = _BV(WGM10);  // wawe generation mode
    TCCR1A |= _BV(COM1A0) | _BV(COM1A1) |
              _BV(COM1B1);  // compare output  is _BV(COM1A0) needed? using also
                            // zero we will invert //! pin 11,12
    TCCR1B = _BV(CS10);     // no prescaler

    // and timer 2
    TCCR2A |= _BV(WGM20);                 // wawe generation mode
    TCCR2A |= _BV(COM2A1) | _BV(COM2B1);  //| _BV(WGM21);  // compare output
                                          //! pin 10 , 9
    TCCR2B |= _BV(CS20);                  // no presc

    // and timer 3
    TCCR3A |= _BV(WGM30);
    TCCR3A |= _BV(COM3A1) | _BV(COM3B1) | _BV(COM3C1);  //! pin 5,2,3
    TCCR3B |= _BV(CS30);

    // and timer 4
    TCCR4A |= _BV(WGM40);
    TCCR4A |= _BV(COM4A1) | _BV(COM4B1) | _BV(COM4C1);  //! pin 6,7,8
    TCCR4B |= _BV(CS40);
// clear all higher bits of output compare for timer
#ifdef RGB_ZERO
    OCR0A = 0;   // pin 13 -> PB7
    OCR1A = 0;   // pin 11 -> pb5
    OCR1BL = 0;  // pin 12 -> pb6
    OCR1CH = 0;
    OCR1CL = 0;
    OCR2A = 0;  // pin 10 -> pb4
    OCR1BH = 0;
    // the LED is connected to pin 13
    // that is the bit 7 of port b, we set it as output
#endif

    DDRB |= _BV(6) | _BV(5) | _BV(4);
    //!      OCR1B OCR1A OCR2A
    //!       pin12 pin11 pin10
    DDRH |= _BV(3) | _BV(4) | _BV(5) | _BV(6);
    DDRE |= _BV(3) | _BV(4) | _BV(5);
    DDRG |= _BV(5);
    memset(&rgb, 0, sizeof(RGB_));
}

uint8_t set_pwm(uint8_t vol, uint8_t n) {
    //! use switch case to set pwm pins
    switch (n) {
        case 0:
            OCR3B = vol;
            break;
        case 1:
            OCR3C = vol;
            break;
        case 2:
            OCR0B = vol;
            break;
        case 3:
            OCR3A = vol;
            break;
        case 4:
            OCR4A = vol;
            break;
        case 5:
            OCR4B = vol;
            break;
        case 6:
            OCR4C = vol;
            break;
        case 7:
            OCR2B = vol;
            break;
        default:
            return ERROR;
            break;
    }
    return 1;
}

void init_timer5(void) {
    const int timer_duration_ms = 100;
    // TCCR0A = 0;  // set entire TCCR0A register to 0
    // TCCR0B = 0;  // same for TCCR0B
    // TCNT0 = 0;   // initialize counter value to 0
    // set compare match register for 2khz increments
    OCR5A = (uint16_t)(15.62 * timer_duration_ms);
    // = (16*10^6) / (2000*64) - 1 (must be <256)
    // turn on CTC mode // clear time on compare
    TCCR5A = 0;
    // 16Mhz /1024 = 16Khz => t = 62.5 µs  !(10^-5)
    TCCR5B |= (1 << WGM52) | (1 << CS52) | (1 << CS50);

    // enable timer compare interrupt
    TIMSK5 |= (1 << OCIE5A);
}

inline void rgb_set(uint8_t red, uint8_t green, uint8_t blue) {
    if (rgb.mode == noeffect) {
        // set each output compare register with right value for the color
        OCR1A = (uint8_t)green;       // green
        OCR1B = (uint8_t)255 - blue;  // red
        OCR2A = (uint8_t)255 - red;   // blue
    }
}
inline void rgb_set_override(uint8_t red, uint8_t green, uint8_t blue) {
    // set each output compare register with right value for the color
    OCR1A = (uint8_t)green;       // green
    OCR1B = (uint8_t)255 - blue;  // blue
    OCR2A = (uint8_t)255 - red;   // red
}
void effect_police(void) {
    // just set various values for using police effect
    rgb.mode = police;
    rgb.red = 2;
    rgb.blue = 250;
    rgb.compare = 10;
}
void effect_stop(void) {
    rgb.mode = noeffect;  // no need for comments
}

void effect_rotate(void) {
    // not used in reality, was a simple colors flow mode
    rgb.mode = rotate;
    rgb.red = 80;
    rgb.green = 160;
    rgb.blue = 240;
}

// timer related

uint8_t passaggio;
void rgb_timer(void) {
    // this helps the effects
    if (rgb.mode != noeffect) {
        //! if some effects are enabled increase rgb time and go inside the
        //! alive mode
        rgb.time++;
        //! if we are in free mode we can just keep those color fixed.. no read
        //! need to do this and not used in reality
        if (rgb.mode == keep) {
            rgb_set_override(rgb.red, rgb.green, rgb.blue);
        } else if (rgb.mode == cpu_usage) {
            if (rgb.time > 600) {
                // if too much time from the last update probably means the pc
                // is off or in stanby so put on a blue light
                rgb_set_override(0, 0, 100);
                rgb.time = 0;
            }
        } else if (rgb.time == rgb.compare) {
            if (rgb.mode == rotate) {  //! flow mode: not used
                rgb_set_override(rgb.red++, rgb.green++, rgb.blue++);
                rgb.time = 0;
            } else if (rgb.mode == police) {
                rgb_set_override(rgb.red, 0, rgb.blue);
                //! apply police mode and switch blue and red values
                passaggio = rgb.blue;
                rgb.blue = rgb.red;
                rgb.red = passaggio;
                rgb.time = 0;
                // printf("change");
            }
        }
    }
}
//! this isr generates an interrupt each 100 ms
//! so it has 1h10hz clock
ISR(TIMER5_COMPA_vect) { rgb_timer(); }
