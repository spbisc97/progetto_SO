#include "argv_usage.h"

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "common.h"
#include "serial.h"

static struct option long_options[] = {
    {"device", required_argument, 0, 'd'},     {"verbose", no_argument, 0, 'v'},
    {"nonotify", no_argument, 0, 'n'},         {"help", no_argument, 0, 'h'},
    {"background", required_argument, 0, 'b'}, {0, 0, 0, 0}};

static const char* shortstr = "d:vnhb";

#define HELP                                    \
    _GREEN_TEXT "smarthouse program" _BLUE_TEXT \
                "\n\
 -h, --help you can display this \n\
 -d, --device followed by the device name \n\
        you can select the device to use \n\
 -n, --nonotify you can exclude the notification\n\
 -b, --background <file.log> to write log to file\n"

int parse_arg(int argc, char** argv) {
    int option_index;
    int c;
    while (1) {
        // int this_option_optind = optind ? optind : 1;
        c = getopt_long(argc, argv, shortstr, long_options, &option_index);
        //! use get option long to be able to use both long and short args ,
        if (verbose) {
            printf("return getopt val per %d \n", option_index);
        }
        if (c == -1) break;

        switch (c) {
            //! just compare the char c we have to the options to start the
            //! things we need
            //! no need to comment the switch case
            case 0:
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;
            case 'v':
                printf("option verbose selected \n");
                verbose = 1;
                break;
            case 'd':
                device = optarg;
                printf("device changed to '%s'\n", device);
                break;
            case 'h':
                printf(HELP);
                //! print help table
                return 0;
                break;
            case 'n':
                printf("remove notification icon\n");
                // needed when we do not want to use yad or we are in a server
                // enviroment (no monitor)
                disable_notification = 1;
                break;
            case 'b':
                //! case b means we are using it in background and we would like
                //! to have a log -> redirect stdout
                //! default into "smarthouse. log file"
                if (optarg) {
                    printf("redirect stdout to file %s\n", optarg);
                    if (freopen(optarg, "a+", stdout)) {
                        printf("\n\n new session \n\n");
                    } else {
                        printf("error in redirecting output");
                    }
                } else {
                    printf("redirect stdout to file %s\n", "smarthouse.log");
                    if (freopen("smarthouse.log", "a+", stdout)) {
                        printf("\n\n new session \n\n");
                    } else {
                        printf("error in redirecting output");
                    }
                }
                break;
            case '?':
                printf("try -h to get help\n");
                return 0;
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }
    if (optind < argc) {
        printf("non-option ARGV-elements: ");
        while (optind < argc) printf("%s ", argv[optind++]);
        printf("\n");
    }
    return 1;
}