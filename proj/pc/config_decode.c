#include "config_decode.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"

char* pot = "pot";        // potentiometer
char* swit = "switch";    // light attached
char* button = "button";  // button attached

char* trimwhitespace(char* str) {
    char* end;

    // Trim leading space
    while (isspace((unsigned char)*str)) str++;

    if (*str == 0)  // All spaces?
        return str;

    // Trim trailing space
    end = str + strlen(str) - 1;
    while (end > str && isspace((unsigned char)*end)) end--;

    // Write new null terminator character
    end[1] = '\0';

    return str;
}

void parse(char* file_name) {
    char line[LINE_BUF];
    char* token;
    int pos;
    FILE* fp;
    fp = fopen(file_name, "r");  // read mode

    if (fp == NULL) {
        perror("Error while opening the file.\n");
        exit(EXIT_FAILURE);
    }
    for (int i = 0; i < 16; i++) {
        //! per azzerare tutti i campi
        memset((void*)&Pot_Buttons_Switch[i], 0, NAME_SIZE + sizeof(uint8_t));
    }
    for (int i = 16; i < 24; i++) {
        memset((void*)&Pot_Buttons_Switch[i], 0, NAME_SIZE + sizeof(uint8_t));
        //! per azzerare tutti i campi ma permettere di resettare il pwm sul pin
        memset((void*)&Pot_Buttons_Switch[i].status, _BV(7), sizeof(uint8_t));
    }

    while (fgets(line, LINE_BUF, fp) > 0) {
#ifdef DEBUG
        printf("%s", line);
#endif  // DEBUG _DEBUG
        //! just scan in sequence
        token = strtok(line, "_");
        while (token != NULL) {
            if (!strcmp(token, "#")) break;
            if (!strcmp(token, pot)) {
                pos = 0;
                if ((token = strtok(NULL, "="))) {
                    pos += (atoi(token) < 8) ? atoi(token) : -1;
                    if ((token = strtok(NULL, "\n")) && pos >= 0) {
                        strncpy((Pot_Buttons_Switch[pos]).name,
                                trimwhitespace(token), NAME_SIZE);
                    }
                }
            } else if (!strcmp(token, button)) {
                pos = 8;
                if ((token = strtok(NULL, "="))) {
                    pos += (atoi(token) < 8) ? atoi(token) : -9;
                    if ((token = strtok(NULL, "\n")) && pos > 0) {
                        strncpy((Pot_Buttons_Switch[pos]).name,
                                trimwhitespace(token), NAME_SIZE);
                    }
                }
            } else if (!strcmp(token, swit)) {
                pos = 16;
                if ((token = strtok(NULL, "="))) {
                    pos += (atoi(token) < 8) ? atoi(token) : -17;
                    if ((token = strtok(NULL, "\n")) && pos > 0) {
                        strncpy((Pot_Buttons_Switch[pos]).name,
                                trimwhitespace(token), NAME_SIZE);
                    }
                }
            }

            // printf("-%s- \n", token);
            token = strtok(NULL, "_");
        }
    }
    fclose(fp);
    if (verbose) {
        int k = 0;
        printf("The contents of %s file are:\n", file_name);
        while (k < 24) {
            if (Pot_Buttons_Switch[k].name[0])
                printf("%s-%d-%s-\n", k > 7 ? swit : pot, k,
                       Pot_Buttons_Switch[k].name);
            k++;
        }
    }
}



#ifdef _DECODE_TESTING
int main() {
    parse("smarthouse.conf");
    int k = 0;
    while (k < 24) {
        if (Pot_Buttons_Switch[k].name[0])
            printf("%s-%d-%s-\n", k > 7 ? swit : pot, k,
                   Pot_Buttons_Switch[k].name);
        k++;
    }
    return 0;
}
#endif