
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define LINE_BUF 256
#define NAME_SIZE 64

struct io_info{
    char name [NAME_SIZE];
    uint8_t status;
} Pot_Buttons_Switch[24];

//io_info Pot_Buttons_Switch[24];

void parse(char* file_name);