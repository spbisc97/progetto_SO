#pragma once


#define HEAD_SETTING "<html><head><meta charset=utf-8/>\
<meta name='viewport' content='width=device-width,initial-scale=1.0'/>\
<!--<script src='assets/jquery.min.js'></script>-->\
<!--<link rel='stylesheet' href='assets/bootstrap.css'/>-->\
<!--<script src='assets/bootstrap.js'></script>--> \
<script src='/javascript.js'></script></head><body>"





#define NAVBAR_SETTING "<nav style='visibility: hidden;' class='navbar navbar-expand-lg navbar-dark bg-dark' >\
<a class='navbar-brand' href='/'>Home</a>\
<button class='navbar-toggler' \
type='button' data-toggle='collapse' \
data-target='#navbarNavAltMarkup' \
aria-controls='navbarNavAltMarkup' \
aria-expanded='false' aria-label='Toggle navigation'> \
<span class='navbar-toggler-icon'></span> </button>\
<div class='collapse navbar-collapse' id='navbarNavAltMarkup'> \
<ul class='navbar-nav ml-auto mt-2 mt-lg-0'> \
<li class='nav-item active'> \
<a class='nav-item nav-link' href='#'>Location : /setting%s</a> \
</li> \
<li class='nav-item active'> \
<a class='nav-item nav-link' href='#'>Time :%s</a> \
</li> \
</ul> \
</div> \
</nav> "

#define AUTOREFRESH "<script type='text/javascript'>\
<!–\
setTimeout(‘location.href=”/setting”‘,1000);\
–>\
</script>"



#define RGB_MODE  "<div class='container'  style='margin: 2rem;' ><h2 id='rgb'>RGB mode: %s</h2>\
<button id='cpu' class='btn btn-light'>Cpu Mode</button>\
<button id='police' class='btn btn-light'>Police Mode</button>\
<button id='color' class='btn btn-light'>Free Set Color</button><input type='color' id='colorset' name='free' value='#f6b73c'></input>\
<button id='rgb-ogg' class='btn btn-light'>OFF</button>\
</div>"

#define SWITCH "<div class='container' style='margin: 2rem;'><H3 style='margin'>SWITCH : %s : %d</H3>\
<button onclick='setswitch(event)' class='btn btn-light'>ON/OFF</button>\
<button onclick='setpwm(event)' class='btn btn-light'>SET</button>\
<input type='range' value='%d' onchange='setpwm(event)' name='pwm-range' id='pwm%d' />\
</div>"

#define BUTTON "<div class='container' style='margin: 2rem;'><H3>%s : %s: %s</H3>\
</div>"

#define POT "<div class='container' style='margin: 2rem;'><H3>POT : %s :  %d</H3>\
</div>"



#define CLOSE_BODY "</body></html>"


