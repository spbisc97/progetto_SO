/** @format */

$(document).ready(function () {
	$("#colorset").attr("value", localStorage.arduino_color);
	$("#colorset")
		.mouseover(function () {
			activecolor = 1000;
		})
		.mouseleave(function () {
			activecolor = 4;
		});
	$("#color").click(function (e) {
		localStorage.arduino_color = $("#colorset").val();
		activecolor = 0;
		$.get("/setting/more/free?value=00" + $("#colorset").val().substring(1, 7), function (
			data,
			status
		) {
			if (status != "success") {
				alert(status);
			} else {
				str = $("#rgb").text();
				$("#rgb").text(str.substring(0, str.lastIndexOf(":")) + ": " + "Static Color");
			}
		});
	});
	$("#rgb-ogg").click(function () {
		$.get("/setting/more/free?value=00000000", function (data, status) {
			if (status != "success") {
				alert(status);
			} else {
				str = $("#rgb").text();
				$("#rgb").text(str.substring(0, str.lastIndexOf(":")) + ": " + "Static Color");
			}
		});
	});
	$("#police").click(function () {
		$.get("/setting/more/police", function (data, status) {
			if (status != "success") {
				alert(status);
			} else {
				str = $("#rgb").text();
				$("#rgb").text(str.substring(0, str.lastIndexOf(":")) + ": " + "Police Mode");
			}
		});
	});
	$("#cpu").click(function () {
		$.get("/setting/more/cpu", function (data, status) {
			if (status != "success") {
				alert(status);
			} else {
				str = $("#rgb").text();
				$("#rgb").text(str.substring(0, str.lastIndexOf(":")) + ": " + "Cpu Usage");
			}
		});
	});
});

function setswitch(event) {
	var value = event.target.nextElementSibling.nextElementSibling;
	var father = event.target.previousElementSibling;
	//alert(value.id.substring(3, 4));
	var led_status = { led: value.id.substring(3, 4), value: 101 };
	//alert(JSON.stringify(status));
	$.get("/setting/more/leds", led_status, function (data, status) {
		if (status != "success") {
			alert("error in set " + value.id.substring(3, 4));
		} else {
			str = father.innerHTML;
			father.innerHTML = str.substring(0, str.lastIndexOf(" ")) + " " + "toggled";
		}
	});
}
function setpwm(event) {
	var father = event.target.previousElementSibling.previousElementSibling;
	if (event.target.id.substring(0, 3) != "pwm") {
		var value = event.target.nextElementSibling;
	} else {
		var value = event.target;
		father = father.previousElementSibling;
	}
	var led_status = { led: value.id.substring(3, 4), value: value.value };
	//alert(value.id.substring(3, 4));
	//alert(JSON.stringify(status));
	$.get("/setting/more/leds", led_status, function (data, status) {
		if (status != "success") {
			alert("error in set " + value.id.substring(3, 4));
		} else {
			str = father.innerHTML;
			father.innerHTML = str.substring(0, str.lastIndexOf(" ")) + " " + led_status.value;
		}
	});
}
