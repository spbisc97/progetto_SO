

#include <libwebsockets.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "argv_usage.h"
#include "common.h"
#include "config_decode.h"
#include "idle.h"
#include "serial.h"
#include "signals.h"
#include "webface.h"

//#define _PID_CONTROL

char *device = "/dev/ttyACM0";
int stat_val;
int disable_notification = 0;
int verbose = 0;
int _web_service = 1;
void *while_web(void *context) {
    while (1) {
        _web_service = lws_service(context, 0);
        if (stop) break;
    }
    lws_context_destroy(context);
    //! i should use the return value ...
    return NULL;
}

int main(int argc, char **argv) {
    if (!parse_arg(argc, argv)) {
        _exit(EXIT_SUCCESS);
    }
    int fd = serial_start(19200, 0, device);
    if (fd < 0) {
        printf(_RED_TEXT "Sorry, no device is connected!" _DEFAULT_TEXT);
        exit(0);
    }
    pid_t pid;
    if (!disable_notification) {
        pid = vfork();
    } else {
        pid = -1;
    }

    if (pid == 0) {
        printf(
            "\nI recommend to keep yad install on your device to easy use this "
            "app \n");
        printf(_GREEN_TEXT "sudo apt install yad\n\n"_DEFAULT_TEXT);
        char *const __argv[] = {
            "nohup",
            "yad",
            "--notification",
            "--command=xdg-open localhost:7681",
            "--image=files/smarthouse.svg",
            "--menu=SmartHouse Menu|Quit!killall smarthouse -s 3|Open "
            "Web!xdg-open localhost:7681|Update Config!killall smarthouse "
            "-s 2",
        };
        execvp("nohup", __argv);
        //! to install yad if not installed
        // printf(
        //     _BLUE_TEXT
        //     "\n#\n# please install  yad for your
        //     platform\n#\n"_DEFAULT_TEXT);
        // char *const _sudo_argv[] = {"xterm", "-e", "sudo apt install yad"};
        // pid = vfork();
        // if (pid == 0) {
        //     execvp("xterm", _sudo_argv);
        //     exit(EXIT_FAILURE);
        // }
        // int wstatus;
        // waitpid(pid, &wstatus, WUNTRACED);
        // // strangely xterm does not give a usable return value
        // if (WIFEXITED(wstatus))
        //     ;
        // execvp("yad", __argv);
        exit(EXIT_FAILURE);
    }
    if (verbose) {
        printf(" pid = %d \n", pid);
    }
    //! get initial cpu info
    cpu_stat_update();
    //! for a baseline
    cpu_stat_update();

    //! start handling signals
    HandleHostSignal();

    //! init values
    uint32_t rgb_usage;
    // int n = 500;

    //! print some info of the process,
    //! how to close and interface info
    process_info();
    //! create the context for the web server
    struct lws_context *context = server_creator();
    if (context == NULL) {
        return -1;
    }
    //! parse all the settings
    parse("smarthouse.conf");
    //! wait all setup before start
    sleep(1);
    pthread_t web_thread;
    pthread_create(&web_thread, NULL, while_web, context);
    while (1) {
        // usleep(50);
        //! update cpu status for arduino rgb
        if (arduino_info.rgb == CPU) {
            cpu_stat_update();
            rgb_usage = semaphore_percent(_stat.usage);
            if (verbose) {
                printf("%f ", _stat.usage);
                printf("%08x \n", rgb_usage);
            }
            rgb_usage |= (uint32_t)(0xdb);
            //! serial write
            serial_hex_write(fd, (uint8_t *)&rgb_usage, 4);
            char buf[MAX_BUF];
            serial_hex_read(fd, (uint8_t *)buf);
            if ((uint8_t)(buf[0]) != 0X79 && verbose) {
                fprintf(STDERR, _RED_TEXT
                        "Comunication error wrong ack \n"_DEFAULT_TEXT);
            }
        } else if (arduino_info.rgb == POLICE && arduino_info.rgb_set) {
            arduino_info.rgb_set = 0;
            serial_hex_write(fd, (uint8_t *)&police_mode, 1);
            char buf[MAX_BUF];
            serial_hex_read(fd, (uint8_t *)buf);
            if ((uint8_t)(buf[0]) != 0X79 && verbose) {
                fprintf(STDERR, _RED_TEXT
                        "Comunication error wrong ack \n"_DEFAULT_TEXT);
            }
        } else if (arduino_info.rgb == FREE && arduino_info.rgb_set) {
            arduino_info.rgb_set = 0;
            uint32_t setcolor =
                (arduino_info.rgb_colors << 8) | (uint32_t)(0xda);
            serial_hex_write(fd, (uint8_t *)&setcolor, 4);
            char buf[MAX_BUF];
            serial_hex_read(fd, (uint8_t *)buf);
            if ((uint8_t)(buf[0]) != 0X79 && verbose) {
                fprintf(STDERR, _RED_TEXT
                        "Comunication error wrong ack \n"_DEFAULT_TEXT);
            }
        }

#ifdef _PID_CONTROL
        if (pid != -1) {
            waitpid(pid, &stat_val, WNOHANG);
            if (verbose) printf("pid status = %d \nyad is online \n", stat_val);
            if (stat_val != 0) {
                pid = -1;
            }
        }
#endif  // DEBUG

        // get potentiometer positions
        uint8_t pos = 0;
        for (; pos < 8; pos++) {
            if (Pot_Buttons_Switch[pos].name[0]) {
                Pot_Buttons_Switch[pos].status = analog_value(fd, pos);
            }
        }
        // get if buttons and lever got pressed
        pos = 8;
        uint8_t values = check_buttons(fd);
        for (; pos < 16; pos++) {
            if (Pot_Buttons_Switch[pos].name[0]) {
                Pot_Buttons_Switch[pos].status = values & _BV((pos - 8));
                //! non è necessario fare lo shift
                //! ma volendo basta sostituirlo con
                //! [ (values & _BV((pos - 8))) >>( pos - 8) ]
            }
        }
        // set switch values
        pos = 16;
        for (; pos < 24; pos++) {
            if (Pot_Buttons_Switch[pos].status & _BV(7)) {
                // if (Pot_Buttons_Switch[pos].name[0]) {
                Pot_Buttons_Switch[pos].status &= ~_BV(7);
                set_pwm(fd, pos - 16, Pot_Buttons_Switch[pos].status);
                //? i shoud use return value
                //}
            }
        }

        if (stop) break;
        fflush(stdout);
    }
    pthread_join(web_thread, NULL);  // todo     aggiungere detouch  e
                                     // todo      in caso join
                                     // todo per il thread
    pthread_detach(web_thread);
    clean_exit(fd);

    //! if pid =! 1 kill the icon process
    pid != -1 ? kill(pid, 3) : pid + 1;
    printf(_RED_TEXT " BYEEEE "_DEFAULT_TEXT);
    return 0;
}