// open
#include "serial.h"

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>   //open
#include <sys/types.h>  //open
#include <termios.h>
#include <unistd.h>

#include "common.h"
#include "config_decode.h"
#include "signals.h"

// #define _HEX
// #define _TEST_SERIAL
// #define _DEBUG
uint8_t errcount;
uint8_t sum;
uint8_t terminator = 0x0f;
uint8_t quit_arduino = 0x3f;
uint8_t police_mode = 0xdc;

int serial_set_interface_attribs(int fd, int speed, int parity) {
    struct termios tty;
    //! here we setup the interface using termios
    memset(&tty, 0, sizeof tty);

    if (tcgetattr(fd, &tty) != 0) {
        fprintf(STDERR, "error %d from tcgetattr", errno);
        return -1;
    }

    switch (speed) {
        case 57600:
            speed = B57600;
            break;
        case 115200:
            speed = B115200;
            break;
        case 19200:
            speed = B19200;
            break;
        case 9600:
            speed = B9600;
            break;
        default:
            fprintf(STDERR, "cannot set baudrate %d\n", speed);
            return -1;
    }

    cfsetospeed(&tty, speed);
    cfsetispeed(&tty, speed);
    cfmakeraw(&tty);

    // enable reading

    tty.c_cflag &= ~(PARENB | PARODD);  //! shut off parity

    tty.c_cflag |= parity;  //! true or false

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;  //@ 8-bit chars

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        fprintf(STDERR, "error %d from tcsetattr", errno);
        return -1;
    }

    return 0;
}

void serial_set_blocking(int fd, int should_block) {
    struct termios tty;
    memset(&tty, 0, sizeof tty);
    if (tcgetattr(fd, &tty) != 0) {
        fprintf(STDERR, "error %d from tggetattr", errno);
        return;
    }

    tty.c_cc[VMIN] = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;  // 0.5 seconds read timeout

    if (tcsetattr(fd, TCSANOW, &tty) != 0)
        fprintf(STDERR, "error %d setting term attributes", errno);
}

int serial_open(const char* name) {
    //! open serial connection
    int fd = open(name, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        fprintf(STDERR, "error %d opening serial, fd %d\n", errno, fd);
    }
    return fd;
}

int serial_read(int fd, char* buf) {
    int n = 1;
    int len = 0;
    while (n > 0) {
        n = read(fd, buf, sizeof(buf));
        buf += n;
        len += n;
    }
    return len;
}

int serial_hex_read(int fd, uint8_t* buf) {
    //! read the interested hex value
    int n = 1;
    int len = 0;
    uint8_t* ptr = buf;
    sum = 0;
    while (n > 0) {
        n = read(fd, buf, 8);
        buf += n;
        len += n;
    }
    if ((n < 0) && (errno == EIO)) {
        perror(strerror(errno));
        fprintf(STDERR, _RED_TEXT "READ Comunication error"_DEFAULT_TEXT);
        stop = 1;
    }
    for (int i = 0; i < (len - 1); i++) {
        sum += ptr[i];
    }

    if (verbose) {
        printf("checksum = %d \n", (sum % 4));
        int i = 0;
        printf("received %dB: ", len);
        do {
            printf("%02x ", ptr[i]);
            i++;
        } while (i < len);
        printf(" \n");
    }  // DEBUG
    //! here we check se il pacchetto è correttamente arrivato
    len -= 1;
    if ((sum % 4) != ptr[len]) {
        return -1;
    }
    return len;
};

void serial_write(int fd, char* buf) {
    int len = strlen(buf);
    write(fd, buf, len);
    write(fd, "\n", 1);
    usleep((len + 25) * 10);  // sleep enough to transmit the 7 plus
                              // receive 25:  approx 10 uS per char transmit}
}

void serial_hex_write(int fd, uint8_t* buf, int len) {
    int i = 0;
    sum = 0;
    while (i < len) {
        sum += buf[i];
        i++;
    };
    sum = (sum % 4);
    write(fd, buf, len);
    write(fd, &sum, 1);
    int ret = write(fd, &terminator, 1);
    if (ret < 0 && errno == EIO) {
        perror(strerror(errno));
        fprintf(STDERR, _RED_TEXT "WRITE Comunication error \n"_DEFAULT_TEXT);
        printf("check if your device is properly connected \n");
        stop = 1;
    }

    usleep((len + 25) * 50);
    if (verbose) {
        i = 0;
        printf("sent: ");
        do {
            printf("%02x ", ((uint8_t*)buf)[i]);
            i++;
        } while (i < len);
        printf("%02x ", sum);
        printf(" \n");
    }
}

int serial_start(int speed, int blocking, char* device) {
    // start serial connection , open device and set attributes fort the
    // interface
    int fd = serial_open(device);
    if (fd < 0) {
        return -1;
    }
    if (serial_set_interface_attribs(fd, speed, 0) < 0) return 0;
    serial_set_blocking(fd, 0);
    return fd;
}

void clean_exit(int fd) {
    //! just say the arduino to quit everything (x2 to be sure)
    serial_hex_write(fd, &quit_arduino, 1);
    serial_hex_write(fd, &quit_arduino, 1);
}

uint8_t analog_value(int fd, uint8_t pot) {
    uint8_t buf[MAX_BUF];
    // build the buf we want to send
    buf[0] = 0xdd;
    buf[1] = pot;
    // write to serial and check for the result
    serial_hex_write(fd, buf, 2);

    int len = serial_hex_read(fd, buf);

    if (len > 2) {
        return buf[1];
    }
    printf(_RED_TEXT "error \n"_DEFAULT_TEXT);
    return 0;
}

uint8_t set_pwm(int fd, uint8_t pos, uint8_t value) {
    uint8_t buf[MAX_BUF];
    buf[0] = 0xde;
    buf[1] = pos;
    buf[2] = value * 2.55;
    if (verbose) {
        printf("pos : %d val %02x \n", buf[1], buf[2]);
    }  // DEBUG
    serial_hex_write(fd, buf, 3);
    int len = serial_hex_read(fd, buf);
    if (len > 3 && buf[0] == 0xde) {
        return buf[1];
    }
    printf("error \n");
    return 0;
}

uint8_t check_buttons(int fd) {
    uint8_t buf[MAX_BUF];
    buf[0] = 0xdf;
    // build the buffer and read the values
    serial_hex_write(fd, buf, 3);
    int len = serial_hex_read(fd, buf);
    if (len > 3) {
        return buf[1];
    }
    printf(_RED_TEXT "error \n"_DEFAULT_TEXT);
    return 0;
}

/**
 *here we can test basic serial interaction
 *you just need to define TEST
 *you can define _HEX if you want hex
 */

void test(int fd, void* line, int line_lenght) {
#ifdef _HEX
    serial_hex_write(fd, line, line_lenght);
#else
    serial_write(fd, "helo!\n");  // send 7 character greeting
#endif
    char buf[MAX_BUF];
#ifdef _HEX
    serial_hex_read(fd, (uint8_t*)buf);
#else
    serial_read(fd, buf);
    printf("%s", buf);
#endif
}

/*
 * examples
 */
uint8_t set_simple_rgb[5] = {0xda, 0x00, 0xff, 0x0f};
uint8_t set_cpu_rgb[5] = {0xdb, 0x00, 0xff, 0x0f};

#ifdef _TEST_SERIAL
int main() {
    int fd = serial_start(19200, 0);
    printf("set interface\n");
    int n = 15;
    sleep(3);
    while (1) {
        test(fd, set_simple_rgb, 4);
        sleep(n);
        n++;
    }
    return 0;
}
#endif