
#include <stdint.h>
#include <strings.h>

#define MAX_BUF 256

char* device;

uint8_t terminator;
uint8_t quit_arduino;
uint8_t police_mode;

int serial_start(int speed, int blocking, char* device);

//file descriptor (arduino port),buf,lunghezza in byte
void serial_hex_write(int fd, uint8_t* buf, int len);
int serial_hex_read(int fd, uint8_t* buf);
void clean_exit(int fd);
void process_info();
uint8_t analog_value( int fd,uint8_t pot);
uint8_t set_pwm(int fd, uint8_t pos, uint8_t value);
uint8_t check_buttons(int fd);
