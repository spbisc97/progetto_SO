#include "signals.h"

#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "common.h"
#include "config_decode.h"
#include "idle.h"
#include "serial.h"

static void signalManage(int pSignal, siginfo_t* info, void* more) {
    printf("signal number: %d\n", pSignal);

    if (pSignal == SIGQUIT) {
        stop = 1;
    } else if (pSignal == SIGHUP) {
    } else if (pSignal == SIGINT) {
        parse("smarthouse.conf");
    }
}

void HandleHostSignal(void) {
    struct sigaction satmp;
    struct sigaction old[10];
    sigemptyset(&satmp.sa_mask);
    satmp.sa_flags = 0;
    satmp.sa_sigaction = signalManage;
    sigaction(SIGINT, &satmp, &old[SIGINT]);
    sigaction(SIGQUIT, &satmp, &old[SIGQUIT]);
    sigaction(SIGHUP, &satmp, &old[SIGHUP]);
    sigaction(SIGTSTP, &satmp, &old[SIGTSTP]);
}

void process_info() {
    printf(_GREEN_TEXT "# please kill me gently when u have to...\n");
    printf("# you can use \n");
    printf(_RED_TEXT " killall smarthouse -s 3  ");
    printf(_GREEN_TEXT "\n# if you need my pid is ");
    printf(_RED_TEXT "% d  \n", getpid());
    printf(_GREEN_TEXT
           "# have fun "_DEFAULT_TEXT
           " \n");
}