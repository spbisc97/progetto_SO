
#include "webface.h"

#include <libwebsockets.h>
#include <signal.h>
#include <stdint.h>
#include <string.h>

#include "common.h"
#include "config_decode.h"
#include "files/defines.h"
#include "idle.h"
#include "serial.h"
#include "signals.h"

char *display_ardu_info[4] = {"Cpu Usage", "Police Mode", "Static Color", 0};

static const struct lws_http_mount mount_test = {
    /* .mount_next */ NULL,
    /* .mountpoint */ "/setting", /* mountpoint URL */
    /* .origin */ NULL,
    /* .def */ NULL,
    /* .protocol */ "lws-setting",
    /* .cgienv */ NULL,
    /* .extra_mimetypes */ NULL,
    /* .interpret */ NULL,
    /* .cgi_timeout */ 0,
    /* .cache_max_age */ 0,
    /* .auth_mask */ 0,
    /* .cache_reusable */ 0,
    /* .cache_revalidate */ 0,
    /* .cache_intermediaries */ 0,
    /* .origin_protocol */ LWSMPRO_CALLBACK,
    /* .mountpoint_len */ 8, /* char count */
    /* .basic_auth_login_file */ NULL,
};

static const struct lws_http_mount mount = {
    /* .mount_next */ &mount_test, /* linked-list "next" */
    /* .mountpoint */ "/",         /* mountpoint URL */
    /* .origin */ "./files",       /* serve from dir */
    /* .def */ "index.html",       /* default filename */
    /* .protocol */ NULL,
    /* .cgienv */ NULL,
    /* .extra_mimetypes */ NULL,
    /* .interpret */ NULL,
    /* .cgi_timeout */ 0,
    /* .cache_max_age */ 0,
    /* .auth_mask */ 0,
    /* .cache_reusable */ 0,
    /* .cache_revalidate */ 0,
    /* .cache_intermediaries */ 0,
    /* .origin_protocol */ LWSMPRO_FILE, /* files in a dir */
    /* .mountpoint_len */ 1,             /* char count */
    /* .basic_auth_login_file */ NULL,
};

int n = 0;

struct pss {
    char path[128];
    int times;
    int budget;
    int content_lines;
};

uint8_t get_request = 0;
char *more;
static int callback_setting(struct lws *wsi, enum lws_callback_reasons reason,
                            void *user, void *in, size_t len) {
    struct pss *pss = (struct pss *)user;
    uint8_t buf[LWS_PRE + 2048];
    uint8_t *start = &buf[LWS_PRE];
    uint8_t *p = start;
    uint8_t *end = &buf[sizeof(buf) - LWS_PRE - 1];
    time_t t;
    int n;
    get_request = 0;
    if (verbose) {
        printf("reason %d", reason);
    }
    switch (reason) {
        case LWS_CALLBACK_HTTP:
            /* in contains the url part after our mountpoint /dyn, if any */
            lws_snprintf(pss->path, sizeof(pss->path), "%s", (const char *)in);
            if (lws_add_http_common_headers(
                    wsi, HTTP_STATUS_OK, "text/html",
                    LWS_ILLEGAL_HTTP_CONTENT_LEN, /* no content len */
                    &p, end))
                return 1;
            if (lws_finalize_write_http_header(wsi, start, &p, end)) return 1;
            pss->times = 0;
            // todo set budget with url  es /setting/3 -> budget=3
            pss->budget = atoi((char *)in + 1);
            // todo this has a strange behaviour
            pss->content_lines = 0;
            if (!pss->budget) pss->budget = 1;
            if (!strcmp("/more/police", pss->path)) {
                arduino_info.rgb = POLICE;
                arduino_info.rgb_set = 1;
                //! pss->budget = 2;
            } else if (!strcmp("/more/cpu", pss->path)) {
                arduino_info.rgb = CPU;
                arduino_info.rgb_set = 0;
                //! pss->budget = 2;
            } else if (!strncmp("/more/free", pss->path, 10)) {
                uint8_t buf[32];
                char *name = "value";
                lws_get_urlarg_by_name(wsi, name, (char *)buf, sizeof(buf));
                uint32_t col = strtol((char *)&(buf[6]), NULL, 16);
                arduino_info.rgb_colors = ((col & 0x000000FF) << 16) |
                                          ((col & 0x0000FF00)) |
                                          ((col & 0x00FF0000) >> 16);
                arduino_info.rgb = FREE;
                arduino_info.rgb_set = 1;
            } else if (!strncmp("/more/leds", pss->path, 10)) {
                uint8_t buf[32];
                lws_get_urlarg_by_name(wsi, "led", (char *)buf, sizeof(buf));
                uint8_t nled = atoi((char *)&buf[4]);
                nled += 16;
                lws_get_urlarg_by_name(wsi, "value", (char *)buf, sizeof(buf));
                if (verbose) {
                    printf("web buf :%s", buf);
                }
                //! here we set value 0-100 last 7 bits
                uint8_t valore = atoi((char *)&buf[6]);
                if ((101 == valore--) && Pot_Buttons_Switch[nled].status > 0) {
                    Pot_Buttons_Switch[nled].status = 0;
                } else {
                    Pot_Buttons_Switch[nled].status = valore;
                }
                //! this is the set value (first bit)
                Pot_Buttons_Switch[nled].status |= _BV(7);  // 0x80
                if (verbose) {
                    printf(" il web ->pos: %d val %02x", nled,
                           Pot_Buttons_Switch[nled].status);
                }  // DEBUG
            }
            lws_callback_on_writable(wsi);
            return 0;
        case LWS_CALLBACK_HTTP_WRITEABLE:
            if (get_request) {
            }
            if (!pss || pss->times > pss->budget) break;
            n = LWS_WRITE_HTTP;
            if (pss->times == pss->budget) n = LWS_WRITE_HTTP_FINAL;
            if (!pss->times) {
                t = time(NULL);

                p += lws_snprintf(
                    (char *)p, end - p, HEAD_SETTING NAVBAR_SETTING RGB_MODE,
                    pss->path, ctime(&t),
                    display_ardu_info[arduino_info.rgb]); /*HEAD_SETTING
                    NAVBAR_SETTING
                                                             AUTOREFRESH*/
            } else {
                /*
                 * after the first time, we create bulk content.
                 *
                 * Again we take care about LWS_PRE valid behind the
                 * buffer we will send.
                 */
                // if (!strcmp(pss->path, "/more")) {
                //     p +=
                //         lws_snprintf((char *)p, end - p, " you are in moreee
                //         ");
                // } else
                //     while (lws_ptr_diff(end, p) > 80)
                //         p +=
                //             lws_snprintf((char *)p, end - p, " %s ",
                //             pss->path);

                // p += lws_snprintf((char *)p, end - p, "<br><br>");
                int pos = 0;
                for (; pos < 8; pos++) {
                    if (Pot_Buttons_Switch[pos].name[0]) {
                        p += lws_snprintf((char *)p, end - p, POT,
                                          Pot_Buttons_Switch[pos].name,
                                          Pot_Buttons_Switch[pos].status);
                    }
                }
                for (; pos < 16; pos++) {
                    if (Pot_Buttons_Switch[pos].name[0]) {
                        p += lws_snprintf((char *)p, end - p, BUTTON,
                                          pos < 12 ? "BUTTON" : "LEVER",
                                          Pot_Buttons_Switch[pos].name,
                                          Pot_Buttons_Switch[pos].status
                                              ? "pressed"
                                              : "unpressed");
                    }
                }
                for (; pos < 24; pos++) {
                    if (Pot_Buttons_Switch[pos].name[0]) {
                        p += lws_snprintf(
                            (char *)p, end - p, SWITCH,
                            Pot_Buttons_Switch[pos].name,
                            Pot_Buttons_Switch[pos].status &
                                ~_BV(7),  // to write level on screen
                            Pot_Buttons_Switch[pos].status &
                                ~_BV(7),  // to keep the level bar at the right
                                          // point
                            pos - 16);    //
                    }
                }
            }

            pss->times++;
            if (lws_write(wsi, (uint8_t *)start, lws_ptr_diff(p, start), n) !=
                lws_ptr_diff(p, start))
                return 1;

            if (n == LWS_WRITE_HTTP_FINAL) {
                if (lws_http_transaction_completed(wsi)) {
                    p += lws_snprintf((char *)p, end - p, CLOSE_BODY);

                    if (verbose) {
                        printf("LWS_CALLBACK_HTTP_WRITEABLE finish \n");
                    }
                    return -1;
                }
            } else
                lws_callback_on_writable(wsi);
            if (verbose) {
                printf("LWS_CALLBACK_HTTP_WRITEABLE \n");
            }
            return 0;
        default:
            break;
    }

    return lws_callback_http_dummy(wsi, reason, user, in, len);
}

static struct lws_protocols protocols[] = {
    {"http", lws_callback_http_dummy, 0, 0, 0, 0, 0},
    {"lws-setting", callback_setting, sizeof(struct pss), 0, 0, 0, 0},
    {NULL, NULL, 0, 0} /* terminator */
};

struct lws_context *server_creator() {
    struct lws_context_creation_info info;
    struct lws_context *context;

    printf(_BLUE_TEXT
           "LWS minimal http server | visit http://localhost:7681 \n");
    memset(&info, 0, sizeof info);
    info.port = 7681;
    info.mounts = &mount;
    info.protocols = protocols;
    context = lws_create_context(&info);
    if (!context) {
        if (verbose) {
            printf("lws init failed\n");
        }
        return NULL;
    }
    return context;
}
