#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

#include "../avr_common/uart.h"

int main(void) {
    DDRE |= (1 << 2);  // Set LED1 as output
    DDRG |= (1 << 0);  // Set LED2 as output

    ADCSRA |=
        (1 << ADPS2) | (1 << ADPS1) |
        (1 << ADPS0);  // Set ADC prescaler to 128 - 125KHz sample rate @ 16MHz

    ADMUX |= (1 << REFS0);  // Set ADC reference to AVCC
    ADMUX |=
        (1 << ADLAR);  // Left adjust ADC result to allow easy 8 bit reading

    // No MUX values needed to be changed to use ADC0

    ADCSRA |= _BV(ADTS1);
    // adts* set to 0              // Set ADC to Free-Running Mode
    ADCSRA |= (1 << ADEN);  // Enable ADC

    ADCSRA |= (1 << ADIE);  // Enable ADC Interrupt
    sei();                  // Enable Global Interrupts

    ADCSRA |= (1 << ADSC);  // Start A2D Conversions

    for (;;)  // Loop Forever
    {
    }
}

ISR(ADC_vect) { printf("-%d\n", ADCH); }