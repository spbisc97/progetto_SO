#include "buttons.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>

//! here we check if buttons are pressed or leveres change their status

#define _MAIN_DEBUG

uint8_t buttons_status = 0;

#define PCINT_MASK 0x0F
#define LEVER_MASK 0x0F
void init_buttons(void) {
    DDRB &= ~PCINT_MASK;    // set PIN_MASK pins as input 0x0F
    PORTB |= PCINT_MASK;    // enable pull up resistors
    PCICR |= (1 << PCIE0);  // set interrupt on change, looking up PCMSK0
    PCMSK0 |= PCINT_MASK;  // set PCINT0 to trigger an interrupt on state change
}

void init_levers(void) {
    DDRL &= ~LEVER_MASK;  // set PIN_MASK pins as input 0x0F
    PORTL |= LEVER_MASK;  // enable pull up resistors
}
void check_levers(void) {
    buttons_status =
        ((~PINL & LEVER_MASK) << 4) | (buttons_status & PCINT_MASK);
}

#ifdef _MAIN_DEBUG
#include "../avr_common/uart.h"
int main(void) {
    printf_init();
    cli();
    init_levers();
    init_buttons();
    sei();
    while (1) {
        _delay_ms(500);
        check_levers();
        printf("buttons: %01x ,%01x ,%01x ,%01x \n", buttons_status & _BV(0),
               ((buttons_status & _BV(1)) >> 1),
               ((buttons_status & _BV(2)) >> 2),
               ((buttons_status & _BV(3)) >> 3));
        printf(
            "lever: %01x ,%01x ,%01x ,%01x \n",
            ((buttons_status & _BV(4)) >> 4), ((buttons_status & _BV(5)) >> 5),
            ((buttons_status & _BV(6)) >> 6), ((buttons_status & _BV(7)) >> 7));
    }
}
#endif

ISR(PCINT0_vect) {
    //! interrupt for ports 50,51,52,53
    buttons_status ^= ~PINB & PCINT_MASK;
    _delay_ms(50);  // in hope to debounce
}
