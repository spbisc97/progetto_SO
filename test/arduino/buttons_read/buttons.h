

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <util/delay.h>

uint8_t buttons_status;

void init_buttons(void);
void init_levers(void);
void check_levers(void);      