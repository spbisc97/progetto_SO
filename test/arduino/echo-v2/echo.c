#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

//!  _BV(n) (1 << n)

#define BAUD 19600
#define MYUBRR (F_CPU / 16 / BAUD - 1)

typedef struct {
    char buf[256];
    uint8_t pos;
    uint8_t txpos;
} UART;

UART uart;

void UART_init(void) {
    // Set baud rate
    UBRR0H = (uint8_t)(MYUBRR >> 8);
    UBRR0L = (uint8_t)MYUBRR;

    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); /* 8-bit data */
    /* Enable     RX    and    TX and  interr-RX and interr-free  and
     * interr-tx-complete  */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0) | _BV(UDRIE0) | _BV(TXCIE0);
    uart.pos = 0;
    uart.txpos = 0;
};

// rx interrupt service routine
ISR(USART0_RX_vect) {
    // read the UART register UDR0
    uint8_t c = UDR0;
    // copies values from UART register to common rx_buffer
    uart.pos++;
    uart.buf[uart.pos] = c;
}
// tx interrupt service routine
ISR(USART0_UDRE_vect) {  // copies values from common tx_buffer to UART register
                         // while there is ancora buffer da scrivere scrivi
    if (uart.buf[uart.pos] == ';') {
        if (uart.txpos < uart.pos) {
            uart.txpos++;
            UDR0 = uart.buf[uart.txpos];
        } else {
            uart.pos = 0;
            uart.txpos = 0;
        }
    }
}

ISR(USART0_TX_vect) {
    uart.buf[uart.pos] = '\n';
    uart.pos++;
    uart.buf[uart.pos] = ';';
}

int main(void) {
    UART_init();
    sei();
    for (;;)
        ;
}