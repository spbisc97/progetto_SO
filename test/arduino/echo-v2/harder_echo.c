#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

//!  _BV(n) (1 << n)
//!  we have a full interrupt based system!!
//! so cool

#define BAUD 19200
#define MYUBRR (F_CPU / 16 / BAUD - 1)
#define MAX_BUF 256

typedef struct {
    char rx_buf[MAX_BUF];
    char tx_buf[MAX_BUF];
    uint8_t rxpos;
    uint8_t rxlen;
    uint8_t txpos;
    uint8_t txlen;
} UART;

UART uart;

void filter(void) {
    UCSR0B &= ~_BV(RXCIE0);
    while (uart.rxpos < uart.rxlen) {
        if (uart.rx_buf[uart.rxpos] == 'l') {
            if (uart.rxpos < MAX_BUF && uart.rx_buf[uart.rxpos + 1] == 'l') {
                uart.tx_buf[uart.txlen++] = 't';
                uart.tx_buf[uart.txlen++] = 'm';
                uart.rxpos += 2;

            } else {
                uart.tx_buf[uart.txlen++] = 't';
                uart.rxpos++;
            }
        } else {
            uart.tx_buf[uart.txlen++] = uart.rx_buf[uart.rxpos++];
        }
    }
    uart.rxpos = uart.rxlen = 0;
    _delay_ms(10);
    UCSR0B |= _BV(UDRIE0) | _BV(RXCIE0);
}

void UART_init(void) {
    uart.rxpos = 0;
    uart.txpos = 0;
    uart.txlen = 0;
    uart.rxlen = 0;
    // Set baud rate
    UBRR0H = (uint8_t)(MYUBRR >> 8);
    UBRR0L = (uint8_t)MYUBRR;

    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); /* 8-bit data */
    /* Enable     RX    and    TX and  interr-RX and interr-free  and
     * interr-tx-complete  */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0);  // | _BV(TXCIE0);
};

// rx interrupt service routine
ISR(USART0_RX_vect) {
    // read the UART register UDR0
    uint8_t c = UDR0;
    // copies values from UART register to common rx_buffer
    uart.rx_buf[uart.rxlen++] = c;
    if (c == '\n' || 0xF == (uint8_t)c) filter();
}
// tx interrupt service routine
ISR(USART0_UDRE_vect) {  // copies values from common tx_buffer to UART register
                         // while there is ancora buffer da scrivere scrivi

    if (uart.txpos < uart.txlen) {
        UDR0 = uart.tx_buf[uart.txpos++];
    } else {
        uart.txpos = uart.txlen = 0;
        UCSR0B &= ~_BV(UDRIE0);
    }
}

// ISR(USART0_TX_vect) {
//     if (uart.txlen == 0) UCSR0B &= ~_BV(UDRIE0);
// }

int main(void) {
    UART_init();
    sei();
    for (;;)
        ;
}