#include "harder_echo.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>  //for port definitions
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

#include "rgb.h"

//!  _BV(n) (1 << n)

#define BAUD 19200
#define MYUBRR (F_CPU / 16 / BAUD - 1)
#define MAX_BUF 256

UART uart;
uint8_t value = 200;

void filter(void) {
    UCSR0B &= ~_BV(RXCIE0);
    while (uart.rxpos < uart.rxlen) {
        // if (uart.rx_buf[uart.rxpos] == 'l') {
        //     if (uart.rxpos < MAX_BUF && uart.rx_buf[uart.rxpos + 1] == 'l') {
        //         uart.tx_buf[uart.txlen++] = 't';
        //         uart.tx_buf[uart.txlen++] = 'm';
        //         uart.rxpos += 2;
        //         effect_police();
        //     } else {
        //         uart.tx_buf[uart.txlen++] = 't';
        //         uart.rxpos++;
        //     }
        // } else
        // if (uart.rx_buf[uart.rxpos] == 'k') {
        //     uart.tx_buf[uart.txlen++] = 'm';
        //     uart.rxpos++;
        //     effect_stop();
        //     rgb_set(20, 225, 10);
        // } else
        if (uart.rxpos < (MAX_BUF - 3) &&
            uart.rx_buf[uart.rxpos] == (uint8_t)0xda) {  //! 0xda -> noeffect
            uart.tx_buf[uart.txlen++] = 'y';  //! 'y'->0x79 mode changed
            uart.rxpos++;
            effect_stop();
            uint8_t red = uart.rx_buf[uart.rxpos++];
            uint8_t green = uart.rx_buf[uart.rxpos++];
            uint8_t blue = uart.rx_buf[uart.rxpos++];
            rgb_set(red, green, blue);
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0xdb) {  //! 0xdb -> cpu_
            uart.tx_buf[uart.txlen++] = 'y';  //! 'y'->0x79 mode changed
            uart.rxpos++;
            // or compare?
            rgb.mode = cpu_usage;
            uint8_t red = uart.rx_buf[uart.rxpos++];
            uint8_t green = uart.rx_buf[uart.rxpos++];
            uint8_t blue = uart.rx_buf[uart.rxpos++];
            rgb_set_override(red, green, blue);
            rgb.time = 0;
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0xdc) {
            //! 0xdc -> start policemode
            effect_police();
            uart.rxpos++;
            rgb.time = 0;
        } else if (uart.rx_buf[uart.rxpos] == (uint8_t)0x3f) {
            //! 0x3f -> stop execution
            DDRB = 0;
            cli();
            stop = 1;
        } else {
            uart.tx_buf[uart.txlen++] = uart.rx_buf[uart.rxpos++];
        }
    }
    uart.rxpos = uart.rxlen = 0;
    _delay_ms(10);
    UCSR0B |= _BV(UDRIE0) | _BV(RXCIE0);
}

void UART_init(void) {
    uart.rxpos = 0;
    uart.txpos = 0;
    uart.txlen = 0;
    uart.rxlen = 0;
    // Set baud rate
    UBRR0H = (uint8_t)(MYUBRR >> 8);
    UBRR0L = (uint8_t)MYUBRR;

    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); /* 8-bit data */
    /* Enable     RX    and    TX and  interr-RX and interr-free  and
     * interr-tx-complete  */
    UCSR0B = _BV(RXEN0) | _BV(TXEN0) | _BV(RXCIE0);  // | _BV(TXCIE0);
};

// rx interrupt service routine
ISR(USART0_RX_vect) {
    // read the UART register UDR0
    uint8_t c = UDR0;
    // copies values from UART register to common rx_buffer
    uart.rx_buf[uart.rxlen++] = c;
    if (c == '\n' || 0x0F == (uint8_t)c) filter();
}
// tx interrupt service routine
ISR(USART0_UDRE_vect) {  // copies values from common tx_buffer to UART register
                         // while there is ancora buffer da scrivere scrivi

    if (uart.txpos < uart.txlen) {
        UDR0 = uart.tx_buf[uart.txpos++];
    } else {
        uart.txpos = uart.txlen = 0;
        UCSR0B &= ~_BV(UDRIE0);
    }
}

// ISR(USART0_TX_vect) {
//     if (uart.txlen == 0) {
//         UCSR0B &= ~_BV(UDRIE0) | ~_BV(TXCIE0);
//     }
// }
