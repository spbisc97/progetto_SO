#include "rgb.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

#include "harder_echo.h"
//? this is not possible
//? void* pwm_pins[] = {
//?     &OCR3B,  //! pin 2
//?     &OCR3C,  //! pin 3
//?     &OCR0B,  //! pin 4
//?     &OCR3A,  //! pin 5
//?     &OCR4A,  //! pin 6
//?     &OCR4B,  //! pin 7
//?     &OCR4C,  //! pin 8
//?     &OCR2B,  //! pin 9
//? };
//  in this function we will init some bits for pwm in
//  oc1a oc1b oc2a -> pin 12,11,10 -> B6,7,8
void init_rgb(void) {
    // we will use timer 1
    TCCR1A = _BV(WGM10);                                // wawe generation mode
    TCCR1A |= _BV(COM1A0) | _BV(COM1A1) | _BV(COM1B1);  // compare output
    TCCR1B = _BV(CS10);                                 // no prescaler

    // and timer 2
    TCCR2A |= _BV(WGM20);                 // wawe generation mode
    TCCR2A |= _BV(COM2A1) | _BV(COM2B1);  //| _BV(WGM21);  // compare output
    TCCR2B |= _BV(CS20);                  // no presc
// clear all higher bits of output compare for timer
#ifndef RGB_ZERO
    OCR0A = 0;  // pin 13 -> PB7
    OCR1A = 0;  // pin 11 -> pb5
    OCR1B = 0;  // pin 12 -> pb6
    OCR1CH = 0;
    OCR1CL = 0;
    OCR1C = 0;
    OCR2A = 0;  // pin 10 -> pb4
    // the LED is connected to pin 13
    // that is the bit 7 of port b, we set it as output
#endif

    const uint8_t mask = _BV(6) | _BV(5) | _BV(4);  // _BV(7) |
    //!                   OCR1B    OCR1A    OCR2A   //  OCR0A
    //!                   pin12    pin11    pin10   //  pin13
    // we configure the pin as output
    DDRB |= mask;  // mask; // pin mode
    memset(&rgb, 0, sizeof(RGB_));
}

//! we will use pins from 2 to 9 for pwm control
//! list:
//!   2  -   3  -   4  -   5  -   6  -   7  -   8  -   9
//! OC3B - OC3C - OC0B - OC3A - OC4A - OC4B - OC4C - OC2B

void init_pwm(void) {
    // we will use timer  0
    TCCR0A |= _BV(WGM20);   // wawe generation mode
    TCCR0A |= _BV(COM0B1);  //| _BV(WGM21);  // compare output
    TCCR0B |= _BV(CS20);    // no presc

    // we will use timer 1
    TCCR1A = _BV(WGM10);  // wawe generation mode
    TCCR1A |= _BV(COM1A0) | _BV(COM1A1) |
              _BV(COM1B1);  // compare output  is _BV(COM1A0) needed? using also
                            // zero we will invert
    TCCR1B = _BV(CS10);     // no prescaler

    // and timer 2
    TCCR2A |= _BV(WGM20);                 // wawe generation mode
    TCCR2A |= _BV(COM2A1) | _BV(COM2B1);  //| _BV(WGM21);  // compare output
                                          //! pin 9
    TCCR2B |= _BV(CS20);                  // no presc

    // and timer 3
    TCCR3A |= _BV(WGM30);
    TCCR3A |= _BV(COM3A1) | _BV(COM3B1) | _BV(COM3C1);  //! pin 5,2,3
    TCCR3B |= _BV(CS30);

    // and timer 4
    TCCR4A |= _BV(WGM40);
    TCCR4A |= _BV(COM4A1) | _BV(COM4B1) | _BV(COM4C1);  //! pin 6,7,8
    TCCR4B |= _BV(CS40);
// clear all higher bits of output compare for timer
#ifndef RGB_ZERO
    OCR0A = 0;   // pin 13 -> PB7
    OCR1A = 0;   // pin 11 -> pb5
    OCR1BL = 0;  // pin 12 -> pb6
    OCR1CH = 0;
    OCR1CL = 0;
    OCR2A = 0;  // pin 10 -> pb4
    OCR1BH = 0;
    // the LED is connected to pin 13
    // that is the bit 7 of port b, we set it as output
#endif

    const uint8_t mask = _BV(6) | _BV(5) | _BV(4);  // _BV(7) |
    //!                   OCR1B    OCR1A    OCR2A   //  OCR0A
    //!                   pin12    pin11    pin10   //  pin13
    // we configure the pin as output
    DDRB |= mask;  // mask; // pin mode
    DDRH |= _BV(3) | _BV(4) | _BV(5) | _BV(6);
    DDRE |= _BV(3) | _BV(4) | _BV(5);
    DDRG |= _BV(5);
    memset(&rgb, 0, sizeof(RGB_));
}

void set_pwm(uint8_t vol, uint8_t n) {
    switch (n) {
        case 1:
            OCR3B = vol;
            break;
        case 2:
            OCR3C = vol;
            break;
        case 3:
            OCR0B = vol;
            break;
        case 4:
            OCR3A = vol;
            break;
        case 5:
            OCR4A = vol;
            break;
        case 6:
            OCR4B = vol;
            break;
        case 7:
            OCR4C = vol;
            break;
        case 8:
            OCR2B = vol;
            break;
        default:
            break;
    }
}

void init_timer5(void) {
    const int timer_duration_ms = 100;
    // TCCR0A = 0;  // set entire TCCR0A register to 0
    // TCCR0B = 0;  // same for TCCR0B
    // TCNT0 = 0;   // initialize counter value to 0
    // set compare match register for 2khz increments
    OCR5A = (uint16_t)(15.62 * timer_duration_ms);
    // = (16*10^6) / (2000*64) - 1 (must be <256)
    // turn on CTC mode // clear time on compare
    TCCR5A = 0;
    // 16Mhz /1024 = 16Khz => t = 62.5 µs  !(10^-5)
    TCCR5B |= (1 << WGM52) | (1 << CS52) | (1 << CS50);

    // enable timer compare interrupt
    TIMSK5 |= (1 << OCIE5A);
}

inline void rgb_set(uint8_t red, uint8_t green, uint8_t blue) {
    if (rgb.mode == noeffect) {
        OCR1A = (uint8_t)green;       // green
        OCR1B = (uint8_t)255 - blue;  // red
        OCR2A = (uint8_t)255 - red;   // blue
    }
}
inline void rgb_set_override(uint8_t red, uint8_t green, uint8_t blue) {
    OCR1A = (uint8_t)green;       // green
    OCR1B = (uint8_t)255 - blue;  // blue
    OCR2A = (uint8_t)255 - red;   // red
}
void effect_police(void) {
    rgb.mode = police;
    rgb.red = 2;
    rgb.blue = 250;
    rgb.compare = 10;
}
void effect_stop(void) { rgb.mode = noeffect; }

void effect_rotate(void) {
    rgb.mode = rotate;
    rgb.red = 80;
    rgb.green = 160;
    rgb.blue = 240;
}

// todo timer related

uint8_t passaggio;
void rgb_timer(void) {
    if (rgb.mode != noeffect) {
        rgb.time++;
        if (rgb.mode == keep) {
            rgb_set_override(rgb.red, rgb.green, rgb.blue);
        } else if (rgb.mode == cpu_usage) {
            if (rgb.time > 600) {
                rgb_set_override(0, 0, 100);
                rgb.time = 0;
            }
        } else if (rgb.time == rgb.compare) {
            if (rgb.mode == rotate) {
                rgb_set_override(rgb.red++, rgb.green++, rgb.blue++);
                rgb.time = 0;
            } else if (rgb.mode == police) {
                rgb_set_override(rgb.red, 0, rgb.blue);
                passaggio = rgb.blue;
                rgb.blue = rgb.red;
                rgb.red = passaggio;
                rgb.time = 0;
                // printf("change");
            }
        }
    }
}
//! this isr generates an interrupt each 100 ms
//! so it has 1h10hz clock
ISR(TIMER5_COMPA_vect) { rgb_timer(); }
