#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

#include "../avr_common/uart.h"

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t mode;  // if 0 deactivated
    uint8_t time;
    uint8_t compare;
} RGB_;

RGB_ rgb;

//  in this function we will init some bits for pwm in
//  oc1a oc1b oc2a -> pin 12,11,10 -> B6,7,8
void init_rgb(void) {
    // we will use timer 1
    TCCR1A = _BV(WGM10);                                // wawe generation mode
    TCCR1A |= _BV(COM1A0) | _BV(COM1A1) | _BV(COM1B1);  // compare output
    TCCR1B = _BV(CS10);                                 // no prescaler

    // and timer 2
    TCCR2A |= _BV(WGM20);                 // wawe generation mode
    TCCR2A |= _BV(COM2A1) | _BV(COM2B1);  //| _BV(WGM21);  // compare output
    TCCR2B |= _BV(CS20);                  // no presc
// clear all higher bits of output compare for timer
#ifndef RGB_ZERO
    OCR0A = 0;   // pin 13 -> PB7
    OCR1A = 0;   // pin 11 -> pb5
    OCR1BL = 0;  // pin 12 -> pb6
    OCR1CH = 0;
    OCR1CL = 0;
    OCR2A = 0;  // pin 10 -> pb4
    OCR1BH = 0;
    // the LED is connected to pin 13
    // that is the bit 7 of port b, we set it as output
#endif

    const uint8_t mask = _BV(6) | _BV(5) | _BV(4);  // _BV(7) |
    //!                   OCR1B    OCR1A    OCR2A   //  OCR0A
    //!                   pin12    pin11    pin10   //  pin13
    // we configure the pin as output
    DDRB |= mask;  // mask; // pin mode
    memset(&rgb, 0, sizeof(RGB_));
}

void init_timer5(void) {
    const int timer_duration_ms = 100;
    // TCCR0A = 0;  // set entire TCCR0A register to 0
    // TCCR0B = 0;  // same for TCCR0B
    // TCNT0 = 0;   // initialize counter value to 0
    // set compare match register for 2khz increments
    OCR5A = (uint16_t)(15.62 * timer_duration_ms);
    // = (16*10^6) / (2000*64) - 1 (must be <256)
    // turn on CTC mode // clear time on compare
    TCCR5A = 0;
    // 16Mhz /1024 = 16Khz => t = 62.5 µs  !(10^-5)
    TCCR5B |= (1 << WGM52) | (1 << CS52) | (1 << CS50);

    // enable timer compare interrupt
    TIMSK5 |= (1 << OCIE5A);
}

inline void rgb_set(uint8_t red, uint8_t green, uint8_t blue) {
    OCR1A = (uint8_t)1 + green;   // green
    OCR1B = (uint8_t)255 - blue;  // red
    OCR2A = (uint8_t)255 - red;   // blue
}
void police(void) {
    rgb.mode = 2;
    rgb.red = 2;
    rgb.blue = 250;
    rgb.compare = 10;
}

int main(void) {
    // printf_init(); creates some problem
    init_rgb();
    const uint8_t mask = _BV(6) | _BV(5) | _BV(4);  // _BV(7) |
    //!                   OCR1B    OCR1A    OCR2A   //  OCR0A
    //!                   pin12    pin11    pin10   //  pin13
    // we configure the pin as output
    DDRB |= mask;  // mask; // pin mode

    uint8_t intensityr = 1;
    uint8_t intensityg = 1;
    uint8_t intensityb = 1;
    cli();
    rgb_set(intensityr, intensityg, intensityb);
    _delay_ms(1000);
    init_timer5();
    sei();
    police();
    rgb_set(1, 250, 0);
    while (1) {
        _delay_ms(1000);
        // we can see something like a context switch
    }
}
uint8_t passaggio;
ISR(TIMER5_COMPA_vect) {  // timer0 interrupt 2kHz toggles pin 8
    // generates pulse wave of frequency 2kHz/2 = 1kHz (takes two cycles for
    // // full wave- toggle high then toggle low)
    if (rgb.mode != 0) {
        rgb.time++;
        if (rgb.mode == 1) {
            rgb_set(rgb.red, rgb.green, rgb.blue);

        } else if (rgb.time == rgb.compare) {
            if (rgb.mode == 3) {
                rgb_set(rgb.red, rgb.green, rgb.blue);
            } else if (rgb.mode == 2) {
                rgb_set(rgb.red, 0, rgb.blue);
                passaggio = rgb.blue;
                rgb.blue = rgb.red;
                rgb.red = passaggio;
                // printf("change");
            }
            rgb.time = 0;
        }
    }
}