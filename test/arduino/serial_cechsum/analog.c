#include "analog.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <util/delay.h>

void analog_init(void) {
    //! admux will set the reference
    //! and the pin we are checking

    ADMUX |= (1 << REFS0);  //? reference voltage 5v

    ADCSRA |= _BV(ADEN);  //? enables adc
    // ADCSRA |= _BV(ADIE);  //? enables interrupt

    ADCSRA |= _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);  //! prescaler 128
}
int analog_read(uint8_t channel) {
    ADMUX &= 0xF0;
    ADMUX |= channel & 0x0f;

    ADCSRA |= _BV(ADSC);
    while (ADCSRA & _BV(ADSC)) {
        ;
    }
    return ADCW;
}
#ifdef MAIN_ANALOG_TESTING
#include "../avr_common/uart.h"
int main(void) {
    analog_init();
    printf_init();
    // analog_read(0);
    // sei();
    while (1) {
        _delay_ms(200);
        int value = analog_read(0);
        printf(" -light %d- \n", value);
        value = analog_read(2);
        printf(" -pot %d- \n", value);
    }
}
#endif  // MAIN_ANALOG_TESTING

//! ISR(ADC_vect) {
//!    ADCSRA |= (1 << ADSC);
//!
//!    printf(" -%d- \n", ADCW);
//!    _delay_ms(500);
//!}