#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom2560.h>
#include <util/delay.h>

#include "harder_echo.h"
#include "rgb.h"
int stop = 0;
int main(void) {
    cli();
    // printf_init(); creates some problem
    init_rgb();
    const uint8_t mask = _BV(6) | _BV(5) | _BV(4);  // _BV(7) |
    //!                   OCR1B    OCR1A    OCR2A   //  OCR0A
    //!                   pin12    pin11    pin10   //  pin13
    // we configure the pin as output
    DDRB |= mask;  // mask; // pin mode
    UART_init();
    init_timer5();
    sei();
    _delay_ms(10);

    for (;;) {
        if (stop) break;
    }
}