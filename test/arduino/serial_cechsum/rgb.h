#include <stdint.h>

typedef struct {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint16_t time;
    uint8_t mode;
    uint8_t compare;
} RGB_;

RGB_ rgb;

enum effects { noeffect= 0 ,keep = 1,police = 2,rotate=3, cpu_usage = 4 };

void init_rgb(void);

void init_timer5(void);

void rgb_set(uint8_t red, uint8_t green, uint8_t blue);

void effect_police(void);

void effect_stop(void);

void rgb_set_override(uint8_t red, uint8_t green, uint8_t blue);

void effect_rotate(void);