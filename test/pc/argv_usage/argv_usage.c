#include "argv_usage.h"

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
    if (!parse_arg(argc, argv)) {
        _exit(EXIT_SUCCESS);
    }
}

int parse_arg(int argc, char **argv) {
    int option_index;
    int c;
    while (1) {
        int this_option_optind = optind ? optind : 1;
        c = getopt_long(argc, argv, shortstr, long_options, &option_index);
        printf("return getopt val per %d \n", option_index);
        if (c == -1) break;

        switch (c) {
            case 0:
                printf("option %s", long_options[option_index].name);
                if (optarg) printf(" with arg %s", optarg);
                printf("\n");
                break;
            case 'v':
                printf("option verbose selected \n");
                verbose - 1;
                break;
            case 'd':
                device = optarg;
                printf("option d with value '%s'\n", device);
                break;
            case 'h':
                printf(HELP);
                return 0;
                break;
            case 'n':
                printf("remove notification icon\n");
                nonotify = 1;
                break;
            case '?':
                printf("try -h to get help\n");
                return 0;
                break;
            default:
                printf("?? getopt returned character code 0%o ??\n", c);
        }
    }
    if (optind < argc) {
        printf("non-option ARGV-elements: ");
        while (optind < argc) printf("%s ", argv[optind++]);
        printf("\n");
    }
    return 1;
}