#include <getopt.h>
#include <stdio.h>
#include <unistd.h>

#define HELP "smarthouse program \n\
 -h, --help you can display this \n\
 -d, --device followed by the device name \n\
you can select the device to use \n\
 -n, --nonotify you can exclude the notification\n"

int verbose = 0;
int nonotify = 0;
char* device;
int parse_arg(int argc, char** argv);



static struct option long_options[] = {{"device", required_argument, 0, 'd'},
                                       {"verbose", no_argument, 0, 'v'},
                                       {"nonotify", no_argument, 0, 'n'},
                                       {"help", no_argument, 0, 'h'},
                                       {0, 0, 0, 0}};

static const char* shortstr = "d:vnh";
