#include <unistd.h>
#include <stdint.h>

#define _RED_TEXT "\033[0;31m"
#define _BLUE_TEXT "\033[0;34m"
#define _GREEN_TEXT "\033[0;32m"
#define _DEFAULT_TEXT "\033[0m"

#define STDERR stderr

enum RGB_MODES { CPU, POLICE, FREE };


typedef struct _ARDUINO{
    uint32_t rgb:7;             //!first 7 bit for rgb modes
    uint32_t rgb_set:1;         //!if color are set in case of police or in case of free
    uint32_t rgb_colors:24;     //!rgb colors in case of free
} _ARDUINO;

_ARDUINO arduino_info;
