
#define HEAD_TESTING "<html><head><meta charset=utf-8/>\
<meta name='viewport' content='width=device-width,initial-scale=1.0'/>\
<script src ='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>\
<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'/>\
<script src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js'></script> \
<script src='/javascript.js'></script>\
</head><body>"




#define NAVBAR_TESTING "<nav class='navbar navbar-expand-lg navbar-dark bg-dark'>\
<a class='navbar-brand' href='/'>Home</a>\
<button class='navbar-toggler' \
type='button' data-toggle='collapse' \
data-target='#navbarNavAltMarkup' \
aria-controls='navbarNavAltMarkup' \
aria-expanded='false' aria-label='Toggle navigation'> \
<span class='navbar-toggler-icon'></span> </button>\
<div class='collapse navbar-collapse' id='navbarNavAltMarkup'> \
<ul class='navbar-nav mr-auto mt-2 mt-lg-0'> \
<li class='nav-item active'> \
<a class='nav-item nav-link' href='/testing'>Testing</a> \
</li> \
</ul> \
</div> \
<div class='collapse navbar-collapse' id='navbarNavAltMarkup'> \
<ul class='navbar-nav ml-auto mt-2 mt-lg-0'> \
<li class='nav-item active'> \
<a class='nav-item nav-link' href='#'>Location : /testing%s</a> \
</li> \
<li class='nav-item active'> \
<a class='nav-item nav-link' href='#'>Time :%s</a> \
</li> \
</ul> \
</div> \
</nav>"


#define RGB_MODE  "<div class='container' style='margin: 2rem;' ><h2>RGB mode: %s</h2>\
<a href='/testing/more/cpu'><button class='btn btn-light'>Cpu Mode</button></a>\
<a href='/testing/more/police'><button class='btn btn-light'>Police Mode</button></a>\
<a id='color' href='/testing/more/free'><button  class='btn btn-light'>Free Set Color</button></a><input type='color' id='colorset' name='free' value='#f6b73c'></input>\
</div>"

#define SWITCH "<div class='container' style='margin: 2rem;'><H3>SWITCH : %s : %d </H3>\
<a href='#'><button class='btn btn-light'>ON/OFF</button></a>\
<a href='#'><button class='btn btn-light'>SET</button></a>\
<input type='range' name='pwm-range' id='pwm' />\
</div>"
#define BUTTON "<div class='container' style='margin: 2rem;'><H3>BUTTON : %s: %d</H3>\
</div>"
#define POT "<div class='container' style='margin: 2rem;'><H3>POT : %s : %d</H3>\
</div>"



#define CLOSE_BODY "</body></html>"


