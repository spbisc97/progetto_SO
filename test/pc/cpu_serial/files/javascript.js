/** @format */

$(document).ready(function () {
	$("#colorset").attr("value", localStorage.arduino_color);
	$("#colorset").change(function (e) {
		$("#color").attr(
			"href",
			"/testing/more/free?value=00" + $("#colorset").val().substring(1, 7)
		);
		localStorage.arduino_color = $("#colorset").val();
	});
	$("#color").click(function (e) {
		$("#color").attr(
			"href",
			"/testing/more/free?value=00" + $("#colorset").val().substring(1, 7)
		);
	});
});
