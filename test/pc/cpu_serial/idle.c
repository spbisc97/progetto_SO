#include "idle.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//#define _DEBUG
//#define _IDLE_TEST

void sys_stat(long int cpu[]) {
    FILE* fp = fopen("/proc/stat", "r");
    //!
    //!      /proc/stat is made like this,
    //!
    //!         cpu  user nice system idle iowait irq foftirq
    //!         cpu1  //   //    //    //     //   //     //
    //!         cpu..
    //!         cpuN
    //!
    //!     if you sum every service you get 100% of total
    //!     we are interested in idle% = idle/total
    //!
    int i = 0;
    char str[100];
    fgets(str, 100, fp);
    fclose(fp);
    char* token = strtok(str, d);
    if (token != NULL) {
        while (token != NULL) {
            token = strtok(NULL, d);
            if (token != NULL) {
                cpu[i] = atoi(token);
#ifdef _DEBUG
                printf("%ld  ", cpu[i]);
#endif  // _DEBUG

                i++;
            }
        }
#ifdef _DEBUG
        printf("\n");
#endif  // _DEBUG
    }
}

// we will use auint 32 bit, 8 bit for each
//! | flags | red | green | blue |
// and convert a percentage to green->yellow->red scale
uint32_t semaphore_percent(float usage) {
    uint32_t res = 0;
    uint8_t portable;
    portable = (uint8_t)(usage * 2.55);
    res = (portable << 8) | ((uint8_t)~portable << 16);
    return res;
};

void print_hex(uint32_t rgb) {
    printf("red %d, green %d,blue %d \n", (uint8_t)(rgb >> 8),
           (uint8_t)(rgb >> 16), (uint8_t)(rgb >> 24));
}

void cpu_stat_update() {
    _stat.lastIdle = _stat.idle;
    _stat.lastSum = _stat.sum;
    _stat.sum = _stat.idle = 0;
    sys_stat(_stat.info);
    for (int i = 0; i < 10; i++) {
        _stat.sum += _stat.info[i];
        if (i == 3) {
            _stat.idle += _stat.info[i];
        }
    }
    _stat.usage = 100 - ((_stat.idle - _stat.lastIdle) * 1.0 /
                         (_stat.sum - _stat.lastSum)) *
                            100;
}

#ifdef _IDLE_TEST
int main(int argC, char* argV[]) {
    int times, lag;
    memset(&_stat, 0, sizeof(t_cpu));

    if (argC != 3) {
        times = 100;
        lag = 1;
    } else {
        times = atoi(argV[1]);
        lag = atoi(argV[2]);
    }
    while (times > 0) {
        // token function will get every string on the line separated by " "
        update();
        printf("in uso per il %f %% del tempo. total of %ld info \n",
               _stat.usage, _stat.sum - _stat.lastSum);
        fflush(stdin);
        uint32_t res = semaphore_percent(_stat.usage);
        printf("%x \n", res);
        print_hex(res);
        times--;
        sleep(lag);
    }
    return 0;
}
#endif