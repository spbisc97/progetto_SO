#include <stdint.h>
typedef struct s_cpu t_cpu;
struct s_cpu {
    float usage;
    long idle;
    long lastIdle;
    long sum;
    long lastSum;
    long info[10];
};

t_cpu _stat;

#define d " "
void cpu_stat_update();

void print_hex(uint32_t rgb);

uint32_t semaphore_percent(float usage);