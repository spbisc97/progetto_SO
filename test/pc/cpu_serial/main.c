

#include <libwebsockets.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "common.h"
#include "config_decode.h"
#include "idle.h"
#include "serial.h"
#include "signals.h"
#include "webface.h"

int _web_service = 1;
void *while_web(void *context) {
    while (1) {
        _web_service = lws_service(context, 0);
        if (stop) break;
    }
    lws_context_destroy(context);
    return NULL;
}

int main(int argc, char **argv) {
    int fd = serial_start(19200, 0, DEVICE1);
    if (fd < 0) {
        printf(_RED_TEXT "Sorry, no device is connected!" _DEFAULT_TEXT);
        exit(0);
    }
#ifndef DISABLE_NOTIFICATION
    pid_t pid = vfork();
#else
    pid_t pid = -1
#endif

    if (pid == 0) {
        char *const __argv[] = {
            "nohup",
            "yad",
            "--notification",
            "--command=xdg-open localhost:7681",
            "--image=files/smarthouse.svg",
            "--menu=SmartHouse Menu|Quit!killall smarthouse -s 3|Open "
            "Web!xdg-open localhost:7681|Update Config!killall smarthouse "
            "-s 2",
        };
        execvp("nohup", __argv);
        printf(_BLUE_TEXT
               "\n#\n# please install yad for your platform\n#\n"_DEFAULT_TEXT);
        char *const _sudo_argv[] = {"xterm", "-e", "sudo apt install yad"};
        pid = vfork();
        if (pid == 0) {
            execvp("xterm", _sudo_argv);
            exit(EXIT_FAILURE);
        }
        int wstatus;
        waitpid(pid, &wstatus, WUNTRACED);
        // strangely xterm does not give a usable return value
        if (WIFEXITED(wstatus))
            ;
        execvp("yad", __argv);
        exit(EXIT_FAILURE);
    }
#ifdef _DEBUG
    printf(" pid = %d \n", pid);
#endif
    //! get initial cpu info
    cpu_stat_update();
    //! for a baseline
    cpu_stat_update();

    //! start handling signals
    HandleHostSignal();

    //! init values
    uint32_t rgb_usage;
    // int n = 500;

    //! print some info of the process,
    //! how to close and interface info
    process_info();
    //! create the context for the web server
    struct lws_context *context = server_creator();
    if (context == NULL) {
        return -1;
    }
    //! wait all setup before start
    sleep(2);
    pthread_t web_thread;
    pthread_create(&web_thread, NULL, while_web, context);
    while (1) {
        usleep(500);
        //! update cpu status for arduino rgb
        if (arduino_info.rgb == CPU) {
            cpu_stat_update();
            rgb_usage = semaphore_percent(_stat.usage);
#ifdef _DEBUG
            printf("%f ", _stat.usage);
            printf("%08x \n", rgb_usage);
#endif
            rgb_usage |= (uint32_t)(0xdb);
            //! serial write
            serial_hex_write(fd, (uint8_t *)&rgb_usage, 4);
            char buf[MAX_BUF];
            serial_hex_read(fd, (uint8_t *)buf);
        } else if (arduino_info.rgb == POLICE && arduino_info.rgb_set) {
            arduino_info.rgb_set = 0;
            serial_hex_write(fd, (uint8_t *)&police_mode, 1);
            char buf[MAX_BUF];
            serial_hex_read(fd, (uint8_t *)buf);
        } else if (arduino_info.rgb == FREE && arduino_info.rgb_set) {
            arduino_info.rgb_set = 0;
            uint32_t setcolor =
                (arduino_info.rgb_colors << 8) | (uint32_t)(0xda);
            serial_hex_write(fd, (uint8_t *)&setcolor, 4);
            char buf[MAX_BUF];
            serial_hex_read(fd, (uint8_t *)buf);
        }

        // get potentiometer positions
        // get if buttons got pressed
        // set switch values

        if (stop) break;
        // serial_hex_write(fd, NULL, 0);
    }
    pthread_join(web_thread, NULL);  // todo     aggiungere detouch  e
                                     // todo      in caso join
                                     // todo per il thread
    pthread_detach(web_thread);
    clean_exit(fd);
    //! if pid =! 1 kill the icon process
    pid != -1 ? kill(pid, 3) : pid + 1;
    printf(_RED_TEXT " BYEEEE "_DEFAULT_TEXT);
    return 0;
}