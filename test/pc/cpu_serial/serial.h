
#include <stdint.h>

#define MAX_BUF 256
#define DEVICE1 "/dev/ttyACM0"

uint8_t terminator;
uint8_t quit_arduino;
uint8_t police_mode;

int serial_start(int speed, int blocking, char* device);

//file descriptor (arduino port),buf,lunghezza in byte
void serial_hex_write(int fd, uint8_t* buf, int len);
int serial_hex_read(int fd, uint8_t* buf);
void clean_exit(int fd);
void process_info();


