#include <libwebsockets.h>
#include <signal.h>
#include <string.h>

#include "more.c"

/**
 *!main to test libwebsocket
 */

int main(int argc, const char **argv) {
    // handler section
    if (!install_handler()) {
        exit(0);
    }
    struct lws_context *context = server_creator();
    if (context == NULL) {
        return -1;
    }
    while (n >= 0 && !interrupted) {
        n = lws_service(context, 0);
        printf(" -%s- ", more);
        fflush(stdout);
    }

    lws_context_destroy(context);
    return 0;
}