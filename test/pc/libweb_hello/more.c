
#include <libwebsockets.h>
#include <signal.h>
#include <string.h>

#include "files/defines.h"

static const struct lws_http_mount mount_test = {
    /* .mount_next */ NULL,
    /* .mountpoint */ "/testing", /* mountpoint URL */
    /* .origin */ NULL,
    /* .def */ NULL,
    /* .protocol */ "lws-testing",
    /* .cgienv */ NULL,
    /* .extra_mimetypes */ NULL,
    /* .interpret */ NULL,
    /* .cgi_timeout */ 0,
    /* .cache_max_age */ 0,
    /* .auth_mask */ 0,
    /* .cache_reusable */ 0,
    /* .cache_revalidate */ 0,
    /* .cache_intermediaries */ 0,
    /* .origin_protocol */ LWSMPRO_CALLBACK,
    /* .mountpoint_len */ 8, /* char count */
    /* .basic_auth_login_file */ NULL,
};

static const struct lws_http_mount mount = {
    /* .mount_next */ &mount_test, /* linked-list "next" */
    /* .mountpoint */ "/",         /* mountpoint URL */
    /* .origin */ "./files",       /* serve from dir */
    /* .def */ "index.html",       /* default filename */
    /* .protocol */ NULL,
    /* .cgienv */ NULL,
    /* .extra_mimetypes */ NULL,
    /* .interpret */ NULL,
    /* .cgi_timeout */ 0,
    /* .cache_max_age */ 0,
    /* .auth_mask */ 0,
    /* .cache_reusable */ 0,
    /* .cache_revalidate */ 0,
    /* .cache_intermediaries */ 0,
    /* .origin_protocol */ LWSMPRO_FILE, /* files in a dir */
    /* .mountpoint_len */ 1,             /* char count */
    /* .basic_auth_login_file */ NULL,
};

static int interrupted;

int n = 0;

void static handler(int signum, siginfo_t *siginfo, void *more) {
    n = signum;
    interrupted = 1;
}

int install_handler() {
    struct sigaction new_action, old_action;
    new_action.sa_sigaction = handler;
    if (sigaction(SIGINT, &new_action, &old_action)) {
        printf("error");
        return 0;
    }
    return 1;
}

struct pss {
    char path[128];
    int times;
    int budget;
    int content_lines;
};
char *more;
static int callback_testing(struct lws *wsi, enum lws_callback_reasons reason,
                            void *user, void *in, size_t len) {
    struct pss *pss = (struct pss *)user;
    uint8_t buf[LWS_PRE + 2048];
    uint8_t *start = &buf[LWS_PRE];
    uint8_t *p = start;
    uint8_t *end = &buf[sizeof(buf) - LWS_PRE - 1];
    time_t t;
    int n;
    printf("reason %d", reason);
    switch (reason) {
        case LWS_CALLBACK_HTTP:
            /* in contains the url part after our mountpoint /dyn, if any */
            lws_snprintf(pss->path, sizeof(pss->path), "%s", (const char *)in);
            if (lws_add_http_common_headers(
                    wsi, HTTP_STATUS_OK, "text/html",
                    LWS_ILLEGAL_HTTP_CONTENT_LEN, /* no content len */
                    &p, end))
                return 1;
            if (lws_finalize_write_http_header(wsi, start, &p, end)) return 1;
            pss->times = 0;
            pss->budget = atoi((char *)in + 1);
            pss->content_lines = 0;
            if (!pss->budget) pss->budget = 10;
            /* write the body separately */
            lws_callback_on_writable(wsi);
            return 0;
        case LWS_CALLBACK_HTTP_WRITEABLE:
            if (!pss || pss->times > pss->budget) break;
            n = LWS_WRITE_HTTP;
            if (pss->times == pss->budget) n = LWS_WRITE_HTTP_FINAL;
            if (!pss->times) {
                t = time(NULL);

                p += lws_snprintf((char *)p, end - p,
                                  HEAD_TESTING NAVBAR_TESTING CLOSE_BODY,
                                  pss->path, ctime(&t));
            } else {
                /*
                 * after the first time, we create bulk content.
                 *
                 * Again we take care about LWS_PRE valid behind the
                 * buffer we will send.
                 */
                more = (strlen(pss->path) > 4) ? pss->path : more;
                if (!strcmp(pss->path, "/more")) {
                    p +=
                        lws_snprintf((char *)p, end - p, " you are in moreee ");
                } else
                    while (lws_ptr_diff(end, p) > 80)
                        p +=
                            lws_snprintf((char *)p, end - p, " %s ", pss->path);

                p += lws_snprintf((char *)p, end - p, "<br><br>");
            }

            pss->times++;
            if (lws_write(wsi, (uint8_t *)start, lws_ptr_diff(p, start), n) !=
                lws_ptr_diff(p, start))
                return 1;

            if (n == LWS_WRITE_HTTP_FINAL) {
                if (lws_http_transaction_completed(wsi)) {
                    printf("LWS_CALLBACK_HTTP_WRITEABLE finish \n");
                    return -1;
                }
            } else
                lws_callback_on_writable(wsi);
            printf("LWS_CALLBACK_HTTP_WRITEABLE \n");
            return 0;

        default:
            break;
    }

    return lws_callback_http_dummy(wsi, reason, user, in, len);
}

static struct lws_protocols protocols[] = {
    {"http", lws_callback_http_dummy, 0, 0},
    {"lws-testing", callback_testing, sizeof(struct pss), 0},
    {NULL, NULL, 0, 0} /* terminator */
};

struct lws_context *server_creator() {
    struct lws_context_creation_info info;
    struct lws_context *context;
    printf("LWS minimal http server | visit http://localhost:7681 \n");
    memset(&info, 0, sizeof info);
    info.port = 7681;
    info.mounts = &mount;
    info.protocols = protocols;
    context = lws_create_context(&info);
    if (!context) {
        lwsl_err("lws init failed\n");
        return NULL;
    }
    return context;
}
