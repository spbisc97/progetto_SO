// open
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>   //open
#include <sys/types.h>  //open
#include <termios.h>
#include <unistd.h>

#define MAX_BUF 256
#define DEVICE "/dev/ttyACM0"

#define _HEX
#define _TEST_SERIAL
#define _DEBUG

int serial_set_interface_attribs(int fd, int speed, int parity) {
    struct termios tty;

    memset(&tty, 0, sizeof tty);

    if (tcgetattr(fd, &tty) != 0) {
        printf("error %d from tcgetattr", errno);
        return -1;
    }

    switch (speed) {
        case 57600:
            speed = B57600;
            break;
        case 115200:
            speed = B115200;
            break;
        case 19200:
            speed = B19200;
            break;
        case 9600:
            speed = B9600;
            break;
        default:
            printf("cannot set baudrate %d\n", speed);
            return -1;
    }

    cfsetospeed(&tty, speed);

    cfsetispeed(&tty, speed);

    cfmakeraw(&tty);

    // enable reading

    tty.c_cflag &= ~(PARENB | PARODD);  // shut off parity

    tty.c_cflag |= parity;  // true or false

    tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;  // 8-bit chars

    if (tcsetattr(fd, TCSANOW, &tty) != 0) {
        printf("error %d from tcsetattr", errno);
        return -1;
    }

    return 0;
}

void serial_set_blocking(int fd, int should_block) {
    struct termios tty;
    memset(&tty, 0, sizeof tty);
    if (tcgetattr(fd, &tty) != 0) {
        printf("error %d from tggetattr", errno);
        return;
    }

    tty.c_cc[VMIN] = should_block ? 1 : 0;
    tty.c_cc[VTIME] = 5;  // 0.5 seconds read timeout

    if (tcsetattr(fd, TCSANOW, &tty) != 0)
        printf("error %d setting term attributes", errno);
}

int serial_open(const char* name) {
    int fd = open(name, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0) {
        printf("error %d opening serial, fd %d\n", errno, fd);
    }
    return fd;
}

int serial_read(int fd, char* buf) {
    int n = 1;
    int len = 0;
    while (n > 0) {
        n = read(fd, buf, sizeof(buf));
        buf += n;
        len += n;
    }
    return len;
}

int serial_hex_read(int fd, uint8_t* buf) {
    int n = 1;
    int len = 0;
    while (n > 0) {
        n = read(fd, buf, sizeof(buf));
        buf += n;
        len += n;
    }
#ifdef _DEBUG
    int i = 0;
    printf("received %dB: ", len);
    do {
        printf("%02x ", buf[i]);
        i++;
    } while (i < len);
    printf(" \n");
#endif  // DEBUG
    return len;
};

void serial_write(int fd, char* buf) {
    int len = strlen(buf);
    write(fd, buf, len);
    write(fd, "\n", 1);
    usleep((len + 25) * 100);  // sleep enough to transmit the 7 plus
                               // receive 25:  approx 100 uS per char transmit}
}

void serial_hex_write(int fd, uint8_t* buf, int len) {
    write(fd, buf, len);
    uint8_t terminator = 0x0F;
    write(fd, &terminator, 1);
    usleep((len + 25) * 100);
#ifdef _DEBUG
    int i = 0;
    printf("sent: ");
    do {
        printf("%02x ", ((uint8_t*)buf)[i]);
        i++;
    } while (i < 5);
    printf(" \n");
#endif
}

int serial_start(int speed, int blocking) {
    int fd = serial_open(DEVICE);
    if (fd < 0) {
        return -1;
    }
    if (serial_set_interface_attribs(fd, speed, 0) < 0) return 0;
    serial_set_blocking(fd, 0);
    return fd;
}

/**
 *here we can test basic serial interaction
 *you just need to define TEST
 *you can define _HEX if you want hex
 */
void test(int fd, void* line, int line_lenght) {
    serial_hex_write(fd, line, line_lenght);
    char buf[MAX_BUF];
    serial_hex_read(fd, (uint8_t*)buf);
}

/*
 * examples
 */
uint8_t set_simple_rgb[5] = {0xda, 0x00, 0xff, 0x0f, 0x0a};
uint8_t set_cpu_rgb[5] = {0xdb, 0x00, 0xff, 0x0f, 0x0a};

#ifdef _TEST_SERIAL
int main() {
    int fd = serial_start(19200, 0);
    printf("set interface\n");
    int n = 15;
    sleep(3);
    while (1) {
        test(fd, set_simple_rgb, 5);
        sleep(n);
        n++;
    }
    return 0;
}
#endif